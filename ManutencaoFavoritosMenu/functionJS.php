<!-- 
    Atualizado função favoriteclick
onde não estava atualizado o obj ao favoritar
-->
<script>
	
/**
 * Adiciona o objeto obj aos favoritos.
 * @param {Object} obj - item do menu
 */	
function favoriteclick(obj){

var id = "";
// recebe o id se o elemento pai for fav
// @id recebe o nome da aplicacao
if($(obj).parent().attr('id').substr(0,3) == 'fav'){
    id = $(obj).parent().attr('id').substr(3);
}else{
    id = $(obj).parent().attr('id');	
}
if (window["lock_" + id]) {
    return false;
}
window["lock_" + id] = true;

fav = 0;

if($(obj).hasClass('fa-star-o')){
    fav = 1;
}else {
    fav = 0;
}
//Insere o icone ativado se a aplicacao for do tipo favorita
$('a#fav'+id+'>i[fav=true]').toggleClass("fa-star fa-star-o");
$('a#'+id+'>i[fav=true]').toggleClass("fa-star fa-star-o");
    
    $.ajax({
        url: "../favorites",
        type: "GET",
        data: { menu_id: id, isfavorite: fav },
        dataType: "html",
        success:function(data){
            if(fav == 1){
                
                item = $($(obj).parent().parent()[0].outerHTML);
                item.children('a').attr('id', 'fav'+ item.children('a').attr('id'));
                item.children('a').children('i:first').click(function(event){
                    event.stopPropagation();
                    favoriteclick(this);	
                });
                //Classe com nome incorreto
                //$('.favorite-menu>ul').append(item);
                $('.favorite-teste>ul').append(item);	
                //console.log("enviou data:"+item);
                

            }else if(fav == 0){
                $('a#fav'+id).parent().slideUp('fast');
                //console.log("deletou data:"+item);
            }
            window["lock_" + id] = false;
        },
        error:function(data){
            console.log(data);
            window["lock_" + id] = false;
        }

    });

}</script>
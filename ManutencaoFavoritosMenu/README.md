## Corrigir bugs do menu aba favoritos 
###( Sem conhecer o projeto) Aplicações envolvidas
* uService
* uAdmin_v3_chat_bkp/ ("recursive__menuPHP")


### Requisitos:
* Erro ao favoritar aplicação, é esperado que somente ela seja favoritada.
* Listagem das aplicações favoritadas com erro. (duplicação)
* Aplicações favoritadas, algumas não estão com a estrela marcada na aba favoritos
* Aplicações que não são marcadas como favoritas estão aparecendo na listagem

### Métricas:
* Listagem dos requisitos e entendimento do problema: 30minutos
* Entendimento do sistema e fluxo de dados: 3horas
* Implementação da correção: 2horas
* Testes: 2horas
* Total: 7h30

#### Att metricas pós analise
* Listagem dos requisitos e entendimento do problema: 3horas
	* Especificação do problema no git
* Entendimento do sistema e fluxo de dados: 2horas
	* intereção de classes e funções multiplas 
* Implementação da correção: 20horas

* Testes: 4horas
* Total: 29h30

### Melhorias 
* Realizado documentação das funções revisadas no projeto 

#### Feedback 
* Estimado :7h30 para 29h30
* Realizado em : 
- - meio periodo 29/03 : 4h
- - meio periodo 01/04: 4h
- - completo em 02/04: 8h
- - 03/04: 1h

- - total: 19h

#### Correção pós alterações
- 10h as 16h 03/04
- - total: 24h
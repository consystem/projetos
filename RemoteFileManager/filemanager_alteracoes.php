<?php
	include 'LogException.php';

/* Classe RemoteFileManager que realiza as transações de arquivos utilizando ssh e ftp
 * @author Bruno Prece, Danilo Rufino
 *
 * @param $conn_tenant, conexão com tenant par apoder inserir na tabela de log
 * @param $table, o nome da tabela de log
 *
 * @link https://secure.php.net/manual/pt_BR/language.exceptions.extending.php
 * @since 02/03/2018
 * @version 1.0.2
 * */
class RemoteFileManager {
	private $ftp_params;
	private $conn_tenant;
	private $app_orign;
	public function __construct($conn_tenant, $ftp_params, $local_config, $app_orign = "RemoteFileManager") {
		$this->ftp_params = $ftp_params;
		$this->conn_tenant = new PDO ($conn_tenant);
		$this->app_orign = $app_orign;														
		$this->prefix = "sshpass -p ".$local_config['password']." ssh -o StrictHostKeyChecking=no -qt ".$local_config['login']."@localhost";
	}
	
	/* Desconstrutor padrao
	 * @author Bruno Prece
	 * Fecha as conexoes abertas ao banco de dados
	 *
	 * @link http://php.net/manual/pt_BR/language.oop5.decon.php
	 * @since 02/03/2018
	 * @version 1.0.2
	 * */
	function __destruct(){
		$this->conn_tenant = null;
	}
	
	/* getFiles
	 * Retorna os arquivos de um diretório com possibilidade de passar uma extensão
	 * e pegar somente os arquivos daquela extensao.
	 * 
	 * @param $remote_path, caminho do diretório remoto
	 * @param $fileType, (opcional) extensão dos arquivos
	 * 
	 * @return $filtered, lista filtrada dos arquivos no diretório.
	 * @since 02/03/2018
	 * @version 1.0.2
	 */
	public function getFiles($remote_path, $fileType = false) {
		$files = '';
		
		$port = ($this->ftp_params['port'] != "" ? "-p ".$this->ftp_params['port'] :"" );

		if (! $fileType) {
			$files = shell_exec("$this->prefix \"sshpass -p ".$this->ftp_params['password']." ssh $port -o StrictHostKeyChecking=no -qt ".$this->ftp_params['login']."@".$this->ftp_params['host'].
					" 'ls -tr $remote_path' \"");
		} else {
			$files = shell_exec("$this->prefix \"sshpass -p ".$this->ftp_params['password']." ssh $port -o StrictHostKeyChecking=no -qt ".$this->ftp_params['login']."@".$this->ftp_params['host'].
					" 'ls -tr $remote_path | grep .$fileType' \"");
		}
		$files = explode("\n", $files);
		$filtered = array_filter($files, function($var){return !empty($var);} );

		return $filtered;
	}
	
	/* getFileList
	 * Retorna os arquivos de um diretório e de todos seus subdiretórios.
     * 
	 * 
	 * @param $remote_path, caminho do diretório remoto
	 * 
	 * @return $filtered, lista filtrada dos arquivos no diretório.
     * Formato da lista:
     * Path de um diretório seguido de itens do diretorio. Exemplo:
     * "/home/cargaxml/diretorioteste/subdir/subsubdir: "
     * "-rw-r--r-- 3,0K Mar 19 14:30 smalfile.dat"
     * "-rw-r--r-- 24M Mar 19 14:30 output.dat"
     *  direito de acesso, size, mes, dia, hora, nome.
     * Caso existam subdiretórios o programa ira lista o seu path e depois seus itens como acima.
	 * @since 20/03/2018
	 * @version 1.0.3
	 */
	public function getFileList($remote_path) {
		$files = '';
        //Explicação do comando awk:
        //awk /\//(caso exista uma '/'){print \\$1, \\$2}(imprima a primeira e segunda coluna) 
        //!/\/|total|(^\\\s*\\$)/ Caso não seja '/' ou 'total' ou inicio de linha '^'
        // onde só existe espaço em branco '\s*' até o final da linha '$'
        //{print $1, $5, $6, $7, $8, $9}' imprima as colunas 1 e 5 até 9.
        $parse_command = " | awk '/\//{print \\$1, \\$2} !/\/|total|(^\\\s*\\$)/{print \\$1, \\$5, \\$6, \\$7, \\$8, \\$9}'";
        //l = lista longa, h = size em formato inteligivel, t = por data de alteração, R = listagem recursiva.
        $list_command = "ls -lhtR ";
		$port = ($this->ftp_params['port'] != "" ? "-p ".$this->ftp_params['port'] :"" );
        $exec_command = "$this->prefix \"sshpass -p ".$this->ftp_params['password']." ssh $port -o StrictHostKeyChecking=no -qt ".$this->ftp_params['login']."@".$this->ftp_params['host'].
                " ".$list_command." '$remote_path' ".$parse_command."\"";
		//echo $exec_command;
        $files = shell_exec($exec_command);
       // echo $exec_command;
        //Remove caractere indesejado do arquivo.(^M == \r)
        $files = str_replace("\r", "", $files);
		$files = explode("\n", $files);
        $filter_empty = function($var){
            return !empty($var);
        } ;
		$filtered = array_filter($files, $filter_empty);
		//print_r($filtered);
		return $filtered;
	}
	public function getFileListLocal($local_path){
		$files = '';

		$parse_command = " | awk '/\//{print $1, $2} !/\/|total|(^\s*$)/{print $1, $5, $6, $7, $8, $9}'";

		$list_command = "ls -lhtR ";

		$files = shell_exec( $list_command.$local_path.$parse_command );

		$files = str_replace("\r", "", $files);

		$files = explode("\n", $files);

        $filter_empty = function($var){
            return !empty($var);
        } ;
		$filtered = array_filter($files, $filter_empty);

		return $filtered;
	}
	/* scanFiles
    
	 * Escaneia um diretório e subdiretórios retornando informação abaixo:
     * array contendo elementos do type "file" ou "folder"
     * onde um arquivo file segue o exemplo: 
     *
     * ["name" => nome_do_arquivo,
     * "type" => "file",
     * "path" => caminho_do_arquivo,
     * "size" => tamanho_do_arquivo]
	 * "description" => campo vazio, usado para compatibilidade para listagem na sircp005.
     *
     * Uma folder segue o padrao:
     *
     * "name" => nome_do_diretorio,
     * "type" => "folder",
     * "path" => caminho_do_diretorio,
     * "items" => array contendo items e pastas
	 * "description" => campo vazio, usado para compatibilidade para listagem na sircp005.
	 * @param $remote_path, caminho do diretorio remoto
	 * 
	 * @return $files, estrutura de arquivos listados.
	 * @since 20/03/2018
	 * @version 1.0.3
	 */
     public function scanFiles($remote_path,$local = 0) {
		
		 if($local){
			$unfiltered = $this->getFileListLocal($remote_path);
		}else{
			$unfiltered = $this->getFileList($remote_path);
		}
		 
        $starts_with = function ($var, $text) {
            return substr($var, 0, strlen($text)) === $text;
        };
        //Retorna se é um diretório listado pelo ls "/home/exemplo"
        $temp = function ($var) {return true;};
        $is_dir = function ($value) use(&$starts_with) { return $starts_with($value, "/");};
        //retorna se é um diretório sublistado em outro diretório "drw-r--r--".
        $is_listed_dir = function ($var) use(&$starts_with) { return $starts_with($var, "d");};
        
        //Inicialização de valores usados no loop.
        $files = array();
        $is_main_dir = true;
        $delimiter = " ";
        $pos_size = 1;
        $pos_month = 2;
        $pos_day = 3;
        $pos_time = 4;
        $pos_name = 5;
        $unfiltered[0] = str_replace(": ", "", $unfiltered[0]);
        $current_path = $unfiltered[0];
        $directories = array();
		//echo "<pre>".print_r($unfiltered, true)."</pre>";
        //Inicia de 1 pois o primeiro elemento é o proprio path.
        for($i = 1; $i < count($unfiltered); $i++) {
            $line = $unfiltered[$i];
            if($is_dir($line)) {
                //Prepara para adicionar items ao diretorio
                $is_main_dir = false;
                $current_path = str_replace(": ", "", $line);
                $directories[$current_path]["items"] = array();
                
            } else if($is_listed_dir($line)) {
                
                //separa elementos e prepara para criar elemento diretorio
                $elements = explode($delimiter, $line);
                $dir_path = $current_path."/".$elements[$pos_name];
                if($is_main_dir) {
                    $dir_path = str_replace("//", "/", $dir_path);
                }
                //echo "<pre>".print_r($elements, true)."</pre>";
                $dir_info = array(
                    "name" => $elements[$pos_name],
                    "type" => "folder",
					"path" => $dir_path,
                    "items" => "empty",
					"description" => "",
					"local" => $local
                );
                
                //Adiciona elemento ao diretorio principal ou subdiretorio correspondente.
                $directories[$dir_path] = $dir_info;
                if($is_main_dir) {
                    $files[] = & $directories[$dir_path];
                } else {
                    $directories[$current_path]["items"][] = & $directories[$dir_path];
                }
            } else {
                //Cria elemento com informações do arquivo.
				$elements = explode($delimiter, $line);

				//Retira-se do path a ultima pasta representando
				//a pasta que o usuario obtem
				// ex. ../lib/dados/pw_db_demo/pw_db_demo_demo/demo-demo
				// para /demo-demo
				// alocando como path ficticio para o usuario não ter acesso
				// ao path original do sistema
				$user_directory = explode("/",$current_path);
				//Retira-se do array o nome do arquivo
				//array_pop($user_directory);


                $file = array (
                    "name" => $elements[$pos_name],
                    "type" => "file",
                    "path" => $current_path."/".$elements[$pos_name],
					// "path" => "/home/demo-demo/".$elements[$pos_name],
					"size" => $elements[$pos_size],
					"description" => "",
					"local" => $local
                );
                
                //Atribui arquivo ao diretorio correspondente.
                if($is_main_dir) {
                    $file["path"] = str_replace("//", "/", $file["path"]);
                    $files[] = $file;
                } else {
                    $directories[$current_path]["items"][] = $file;
                }
            }
			
        }
	
        return $files;
    }
	
	/* download
	 * Realiza o Donwload do arquivo
	 *
	 * @param $file, nome do arquivo
	 * @param $local_path,
	 * @param $remote_path, caminho do diretorio remoto
	 *
	 * @return TRUE se o download ocorreu corretamente ou FALSE em caso de falha
	 * @since 02/03/2018
	 * @version 1.0.2
	 */
	public function download($local_path, $remote_path) {
		$local_path = dirname(__FILE__).'/'.$local_path;
		try{
			
			$port = ($this->ftp_params['port'] != "" ? "-P ".$this->ftp_params['port'] :"" );

			$output = shell_exec("$this->prefix 'sshpass -p ".$this->ftp_params['password']." scp ".$port." ".$this->ftp_params['login']."@".$this->ftp_params['host'].":".$remote_path ."  ".$local_path."' ");

			$comando = "$this->prefix 'sshpass -p ".$this->ftp_params['password']." scp ".$port." ".$this->ftp_params['login']."@".$this->ftp_params['host'].":".$remote_path ."  ".$local_path." && echo $?' ";
			//print $comando;
			$arquivo = strrchr($remote_path, "/");
			
			$cmd = "[ -e " . $local_path.'/'.$arquivo . " ]; echo $? ";

			$ret = shell_exec($cmd);

			if($ret != 0){
				throw new LogException($this->conn_tenant, 'sc_log', "$this->app_orign" ,"Falha: nao foi possivel realizar o download de  $remote_path em $local_path  $comando");
			}
		}catch (LogException $e){
			return false;
		}
		return true;

	}
	
	
	
	/* delete
	 * Realiza o Donwload do arquivo
	 *
	 * @param $file, nome do arquivo
	 * @param $local_path,
	 * @param $remote_path, caminho do diretorio remoto
	 *
	 * @return TRUE se o download ocorreu corretamente ou FALSE em caso de falha
	 * @since 02/03/2018
	 * @version 1.0.2
	 */
	public function delete($file){
		$port = ($this->ftp_params['port'] != "" ? "-p ".$this->ftp_params['port'] :"" );

		try{

			$ret = shell_exec("$this->prefix \"sshpass -p ".$this->ftp_params['password']." ssh $port -o StrictHostKeyChecking=no -qt ".$this->ftp_params['login']."@".$this->ftp_params['host'].
					" 'rm -rf $file && echo $?'\"");

			if($ret != 0){
				throw new LogException($this->conn_tenant, 'sc_log', "$this->app_orign" ,"Falha de exclusao: $ret");
			}

		}catch (LogException $e){

			//return FALSE;
		}

		//return TRUE;
	}
	
	
	
	
	/* getContent
	 * Retorna o conteudo de um arquivo, com opção de delimitar a quantidade de caracters lidos, caso queira o arquivo completo
	 * não informar nada no parametro size
	 *
	 * @param $file, caminho do diretorio remoto
	 *
	 * @return $content, conteudo do arquivo.
	 * @since 02/03/2018
	 * @version 1.0.2
	 */
	public function getContent($file){

		$port = ($this->ftp_params['port'] != "" ? "-p ".$this->ftp_params['port'] :"" );

		try {

			$verify = shell_exec("$this->prefix \"sshpass -p ".$this->ftp_params['password']." ssh $port -o StrictHostKeyChecking=no -qt ".$this->ftp_params['login']."@".$this->ftp_params['host'].
					" 'cat $file 2>>/dev/null | wc -l' \"");
			$verify = explode("\n", $verify);

			if($verify[count($verify)-2] == 0){
				throw new LogException($this->conn_tenant, 'sc_log', "$this->app_orign" ,"Falha: não foi possivel buscar o conteúdo de $file.");
			}
			
		} catch (LogException $e) {

			return false;
		}
		
		$content= shell_exec("$this->prefix \"sshpass -p ".$this->ftp_params['password']." ssh $port -o StrictHostKeyChecking=no -qt ".$this->ftp_params['login']."@".$this->ftp_params['host'].
					" 'cat $file'\"");
		return $content;
	}
	
	
}
?>

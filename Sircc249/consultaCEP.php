<?php


class ViaCEP {

    const URL_VIACEP = 'http://viacep.com.br/ws/';
    const METHOD = 'json';
    

    function procuraPorCEP(string $cep = ""){
        $url = self::URL_VIACEP . $cep . '/' . self::METHOD;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($response, true);
        return $data;
    }


}
$a = NEW ViaCEP();
print_r($a->procuraPorCEP('01319000'));


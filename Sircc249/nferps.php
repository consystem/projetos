<?php
class NfeRps{
	private $std;
	/**
	 * Constructor
	 * Recebe um Objeto do tipo StdClass, objeto padr�o php
	 *
	 * @param     StdClass $std
	 * @author    Bruno Prece
	 * @since     30/01/2019
	 * @link      Demanda: 42529
	 * @version   1.0
	 */
	function __construct(StdClass $std){
		$this->std = $std;
	}
	
	/**
	 * toXml
	 * Trandforma o objeto std em XMl recursivamente caminhando pelas propriedades do objeto
	 *
	 * @return	 String, xml do objeto
	 * @author    Bruno Prece
	 * @since     30/01/2019
	 * @link      Demanda: 42529
	 * @version   1.0
	 */
	function toXml(): string{
		$data = json_decode(json_encode($this->std), true);
		$xml_data = new SimpleXMLElement('<Rps></Rps>');
		$this->array_to_xml($data,$xml_data);
		
		// retirando declara��o do documento do meio do evento, pois ja�est� declarado no lote
		return str_replace('<?xml version="1.0"?>','',$xml_data->asXML());
	}
	
	function array_to_xml( $data, &$xml_data ) {
		foreach( $data as $key => $value ) {
			if( is_numeric($key) ){
				$key = 'item'.$key; //dealing with <0/>..<n/> issues
			}
			if( is_array($value) ) {
				$subnode = $xml_data->addChild($key);
				$this->array_to_xml($value, $subnode);
			} else {
				$xml_data->addChild("$key",htmlspecialchars("$value"));
			}
		}
	}
	
}
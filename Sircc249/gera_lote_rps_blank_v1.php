// demanda 42529
// Nicolas Aoki 04/02/2019
// Gera arquivo com inumeras RPS para envio unico
sc_include_library("sys", "dsw", "Sircc249/sircc249.php");
//sc_include_library("sys", "dsw", "efdreinf/UnificaLoteRPS/nferps.php");

	$host = $this->Db->host;

	$host = str_replace(":5432",";port=5432",$host);
	$db= $this->Db->database;
	$username = $this->Db->user;
	$password = $this->Db->password;
try{
	$str_conn = "pgsql:host=$host;port=5432;dbname=$db;user=$username;password=$password";
	$conn = new PDO($str_conn);
} catch (PDOException $e) {
	
	echo 'Connection failed: ' . $e->getMessage();

}
// echo "<pre>";
// print_r($this->Db);
// echo "</pre>";	

if(!empty($_GET['emp'])){
	$filial = $_GET['emp'];
	$sql = "SELECT razao_social,endereco,municipio FROM sx.sirca000 WHERE loja=".$filial;
	$razao = '';
	$endereco = '';
	$municipio = '';
	
	try{
		sc_lookup(rs,$sql,"pw_db_sx");
		$razao = $rs[0][0];
		$endereco = $rs[0][1];
		$municipio = $rs[0][2];
		
		$infoFilial = array(
			'razao' => $razao,
			'endereco' => $endereco,
			'municipio' => $municipio 
    	);
		
		echo json_encode(["sucesso" => $infoFilial]);
	
	}catch(Throwable $e){
		echo json_encode(["sucesso" => "Erro na transmissão".$e]);
	}
}
if(!empty($_GET['filial']) && !empty($_GET['periodo'])){

	$filial = $_GET['filial'];
	$periodo = $_GET['periodo'];
	

	$a = new Sircc249($conn, ["$filial"], $periodo);
	
	

	if(empty($a->path)){
		echo json_encode(["alert" => "Não há RPS neste período "]);
	}else{
		if(empty($a->erroCepIncorretos)){
			echo json_encode(["sucesso" => implode(",",$a->path)]);
		}else{
			$aux = '';
			$aux = $a->erroCepIncorretos;
			
			echo json_encode(["alert" => "Correção necessaria : <a id='rpsResp' href='".implode("<br>",$a->path)."' download>Download</a><br>".implode(",",$a->erroCepIncorretos)]);
		}
	}
	
}

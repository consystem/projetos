<?php
	

include 'nferps.php';


/**
 * Sircc249
 * Captura os dados das notas fiscias por filial e formata no XMl para envio no munic�pio de chapec�
 *
 * @author    Bruno Prece
 * @since     30/01/2019
 * @link      Demanda: 42529
 * @version   1.0
 */
class Sircc249{

	/*Conex�o PDO com o banco*/
	private $conn;
	
	/*array contendo a lista de filiais exemplo [810, 802, 803]*/
	private $filais;
	
	/*Ano e M�s form-to de exemplo '2018-01' */
	private $anomes;
	
	// URL de consulta de CPF
    const URL_VIACEP = 'http://viacep.com.br/ws/';
	
	// Formato de retorno de requisicao do CPF
	const METHOD = 'json';

	// Obtem as identificacoes das RPS com erro de CEP
	public $erroCepIncorretos = array();
	
	//Recebe os nomes de arquivo(s) gerado(s)
	public $path = array();

	/**
	 * Constructor
	 *Captura os dados das notas fiscias por filial e formata no XMl para envio no munic�pio de chapec�
	 *
	 * @param     $conn,  objeto PDO de conex�o com o banco da empresa, banco sx
	 * @param     $filiais, array com os c�digos das filaiis
	 * @param     $anomes, periodo de apura��o dos dados a serem enviados mes/ano
	 *
	 * @author    Bruno Prece
	 * @since     30/01/2019
	 * @link      Demanda: 42529
	 * @version   1.0
	 */
	function __construct(PDO $conn, array $filiais, String $anomes){
		$this->conn 	= $conn;
		$this->filais 	= $filiais;
		$this->anomes 	= $anomes;
		
		$this->init();
	}
	
	/**
	 * Init
	 * Funcao INIT inicializa a execu��o, � o m�todo principal que chama os outros m�todos
	 *
	 * @author    Bruno Prece
	 * @since     30/01/2019
	 * @link      Demanda: 42529
	 * @version   1.0
	 */
	function init(){
		
		//Para cada filial na lista de filiais
		foreach ($this->filais as $filial){
			
			//busca os dados das notas de acordo com a filial passada como par�metro
			$nfs = $this->getData($filial);
			
			$lote = array();
			
			//para cada nota do resultado da consulta do banco
			foreach ($nfs as $nf){
				//echo $this->formatnf($nf);
				
				//aplica a formatacao na nota, transforma de array em um xml formatado
				$lote[] = $this->formatnf($nf);
			}
			//print_r($lote);
			
			//pega um array e se ele tiver mais doq ue 500 intens, divide em v�rios arrays de 500
			//exemplo se o array tem 520 itens, a func��o array_chunk retorna um array de 500 e um de 20
			$lotes = array_chunk($lote, 500);//divide lotes maior do que 500
			//print_r($lotes);
			//die;
			//para cada lote de notas, chama a fun��o newLote que vai gerar o aqrquivo XML em si e vai dar um n�mero par ao lote
			foreach ($lotes as $lote){
				$this->newLot($lote, $filial);
			}
		}		
	}
	
	// Retorna Endereço completo pelo CEP
	function procuraPorCEP($cep = ''){
		//echo $cep . "\n";
		if($cep == ''){
			return null;
		}else{


			$url = self::URL_VIACEP . $cep . '/' . self::METHOD;
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			$response = curl_exec($ch);
			curl_close($ch);
			
			$data = json_decode($response);
			return $data;	
		}
		
	}
	/*n�emro do lote � controlado pelo emissor
	 * desta forma ser� criada uma sequence compartilhada entre as filiais
	 * CREATE SEQUENCE public.sircc249_lote_id
		  INCREMENT 1
		  MINVALUE 1
		  MAXVALUE 9223372036854775807
		  START 1
		  CACHE 1;
	 */
	function getLotNumber(int $filial): int{
		$sql = "SELECT NEXTVAL('sircc249_lote_id');";
		$rs = $this->conn->query($sql);
		$rs = $rs->fetchAll(PDO::FETCH_ASSOC);
		return  $rs[0]['nextval'];
		
	}
	
	/**
	 * getData
	 * Para filial e mes_ano passsada como par�metro, retorna as notas a serem enviadas
	 * 
	 * @return 	  array, retorna o array com o RESULTSET (sesultado da consulta do banco)
	 * @param	  $filial, n�mero da filial
	 * @author    Bruno Prece
	 * @since     30/01/2019
	 * @link      Demanda: 42529
	 * @version   1.0
	 */
	function getData(int $filial): array{
		//try catch para controle de exce��es
		//$rpsTeste2Reais = '';
		try{

			//Feito teste em producao com Nota 14137
			//Retirando-a do envio, para nao gerar problemas
			//de duplicacao
			/*if($this->anomes == '2019-01' && $filial==802){
				$rpsTeste2Reais =  "and a011.nota_fiscal != F;";
			}*/

			$sql = "SELECT
			a011.tipopedi,
			a011.loja,
			a011.nota_fiscal 	AS \"NumeroNf\",  
			a011.serie			AS \"Serie\", 
			6 					AS \"Tipo\",
			a011.dt_emissao||'T'||a011.horanota AS \"DataEmissao\",

			--CASE
			--	WHEN TRIM(a000.cgc) = TRIM(a011.cgc_rg_entrega) THEN '104'
			--	WHEN a011.base_iss+a011.base_retiss = 0 THEN '3'
			--	WHEN a011.base_iss+a011.base_retiss > 0 THEN '101'
			--END 				AS \"NaturezaOperacao\",
			
			a011.base_iss,
			a011.base_retiss,
			a011.outmun_iss,
			a000.cod_regtrib,
			a000.cgc,
			a011.cgc_rg_entrega,
			a011.retencao_iss,
			CASE 
				WHEN a000.cod_regtrib <> '3' THEN 1 --SIM
				WHEN a000.cod_regtrib  = '3' THEN 2 --NAO
			END 				AS \"OptanteSimplesNacional\",
			CASE
				WHEN a518.incentivadorcultural IS NULL THEN 2
				ELSE 1
			END  				AS \"IncentivadorCultural\",
			CASE
				WHEN a011.tipo_registro = 'X' THEN 2
				ELSE 1
			END 				AS \"Status\",
			CASE 
				WHEN a011.tpordser = 'M' 		THEN (a011.totalnf_mo+a011.franquia_mo)
				WHEN a011.tpordser IN('S','D')	THEN (a011.totanota_ef+a011.franquia)
				WHEN a011.tpordser = ' ' 		THEN (a011.totanota_ef)
			END 				AS \"ValorServicos\",
			0.00				AS \"ValorDeducoes\",
			a011.retencao_pis 			AS \"ValorPis\",
			a011.retencao_cofins 		AS \"ValorCofins\",
			CASE 
				WHEN sirnfs.movi IS NOT NULL THEN a011.vlr_irrf
				ELSE 0.00
			END 				AS \"ValorInss\",
			CASE 
				WHEN sirnfs.movi IS NULL THEN a011.vlr_irrf
				ELSE 0::numeric
			END 				AS \"ValorIr\",
			sirnfs.movi,
			a011.codimovi,
			a011.movto_mo,
			a011.retencao_csll 	AS \"ValorCsll\",
			a011.retencao_iss	AS \"IssRetido\",
			a011.valor_iss::numeric		AS \"ValorIss\",
			(a011.retencao_pis + a011.retencao_cofins) AS \"OutrasRetencoes\",
			a011.aliq_iss		AS \"Aliquota\",
			0.00				AS \"DescontoIncondicionado\",
			CASE
				WHEN a011.tpordser = 'M' THEN (a011.vlrdesco_mo + a011.franquia_mo)
				ELSE (a011.descontos + a011.franquia)
			END 				AS \"DescontoCondicionado\",
			a011.codiserv		AS \"ItemListaServico\",


			CASE
				WHEN a011.tipopedi = 'O' THEN left((SELECT array_to_string(array_agg(a605.descserv::TEXT), ',')
												FROM sx.sirca605 a605 where (a605.cdfilial = a011.loja) AND
												(a605.tipopedi = 'P') AND (a605.cdordser = a011.pedido) AND
												(a605.cdsubord = a011.nrcplped)),2000)
				ELSE left((SELECT array_to_string(array_agg(a11b.descricao::TEXT), ',') 
						FROM sx.sirca11b a11b WHERE a11b.loja = a011.loja AND
					a11b.serie = a011.serie AND a11b.nota_fiscal = a011.nota_fiscal),2000)

			END 				AS \"Discriminacao\",
			ba169.cod_ibge 		AS \"FilialCodigoMunicipio\",
			a169.cod_ibge 		AS \"CodigoMunicipio\",
			1058 				AS \"CodigoPais\", --fixado como Brasil
			a000.cgc			AS \"Cnpj\",
			--39834				AS \"InscricaoMunicipal\",
			a000.insc_municipal AS \"InscricaoMunicipal\",
			a011.cgc_rg_entrega AS \"CpfCnpj\",
			a011.nome 			AS \"RazaoSocial\",
			split_part(a011.end_entrega, ',', 1) AS \"Endereco\",
			substring(trim(split_part(a011.end_entrega, ',', 2)), 0, 9) AS \"Numero\",
			split_part(a011.end_entrega, ',', 2) AS \"Complemento\",
			a011.bairro_entrega AS \"Bairro\",
			a011.estado_ef 		AS \"Uf\",
			rpad(a011.cep::text, 5, '0'::text)|| lpad(a011.cep_sufixo::text, 3, '0'::text) AS \"Cep\"
			
			FROM sx.sirca011 a011 JOIN sx.sirca000 a000
				ON(a011.loja = a000.loja) 
			LEFT JOIN (SELECT substring(codiitem, 0, 5)::integer as incentivadorcultural from sx.sirca518  where coditabe = 'SIRNFS' AND codiitem ILIKE '%CUL%') a518
				ON (a518.incentivadorcultural = a011.loja)
			LEFT JOIN sx.sirca169 a169 
				ON((a011.cep::text || lpad(a011.cep_sufixo::text, 3, '0'::text))::numeric = a169.cep)
			LEFT JOIN sx.sirca169 ba169
				ON((a000.cep::text || lpad(a000.cep_sufixo::text, 3, '0'::text))::numeric = ba169.cep)
			LEFT JOIN (SELECT unnest(string_to_array(sirca518.conteudo::text, '#'::text)) AS movi, TRIM(LEADING '0' FROM (substring(sirca518.codiitem,0,5)))as \"loja\"
					FROM sx.sirca518 AS sirca518 WHERE
					sirca518.coditabe::text = 'SIRNFS'::text AND sirca518.codiitem LIKE '%MOVIN%' 
					EXCEPT SELECT TRIM (LEADING '0' FROM (substring(codiitem,0,5))), ''::text AS text FROM sx.sirca518) sirnfs
				ON(a011.loja = sirnfs.loja::numeric) AND ((a011.codimovi = sirnfs.movi::numeric) OR (a011.movto_mo = sirnfs.movi::numeric))
			WHERE ((a011.tpordser IN ('M','S','D')) OR (a011.tpordser = ' ' AND a011.tipo_oper = 'PS')) AND 
			a011.loja = $filial AND to_char(a011.dt_emissao::timestamp with time zone, 'YYYY-MM'::text) = '".$this->anomes."'AND (a011.nota_fiscal = 93686 OR a011.nota_fiscal = 93688) AND a011.serie = '3' "; 
// $rpsTeste2Reais;
			//echo $sql;
			
			$rs = $this->conn->query($sql);
			$rs = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $rs;
		
		}catch (PDOException $e){
			print_r($e);
			print  $e->getMessage();
		} catch (Exception $e) {
			print_r($e);
		}

	}
	
	/**
	 * getFilInf
	 * Retorna os dados basicos da filial
	 *
	 * @return 	  array, retorna o array com o RESULTSET (sesultado da consulta do banco)
	 * @param	  $filial, n�mero da filial
	 * @author    Bruno Prece
	 * @since     30/01/2019
	 * @link      Demanda: 42529
	 * @version   1.0
	 */
	function getFilInf(int $filial): array{
		
		$sql = "SELECT cgc, insc_municipal FROM sx.sirca000 WHERE loja = $filial";
		try {
			$rs = $this->conn->query($sql);
			$rs = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $rs[0];
			
		}catch (PDOException $e){
			print_r($e);
			print  $e->getMessage();
		} catch (Exception $e) {
			print_r($e);
		}
	}
	

	/**
	 * formatnf
	 * Retorna objeto std para ser convertido para xml depois
	 *
	 * @param	  $nota, n�array contendo os dados da nota, � uma linha do banco, um resultado do select
	 * @return 	  String, string com o conte�do da nota convertido para xml 
	 * @author    Bruno Prece
	 * @since     30/01/2019
	 * @link      Demanda: 42529
	 * @version   1.0
	 */
	function formatnf(array $nota): String{
		//para todos os campos, do array da nota, executa o trim automaticamente
		$nota = array_map('trim',$nota);
		
		//transforma um objeto do tipo array, em um objeto do tipo StdClass, objeto padr�o
		//assim, pode-se acessar as propriedade assim $std->minha_variavel
		//ao inv�s de acessar assim $std['minha_variavel']
		$std =  json_decode(json_encode($nota), false);
		
		//cria��o de um objeto j� no formato das tags do xml, onde o objet $Rps ir� receber os valores
		//contidos na nota fiscal
		$Rps		 = new stdclass();
		$Rps->InfRps = new stdclass();
		$Rps->InfRps->IdentificacaoRps = new stdclass();
		$Rps->InfRps->IdentificacaoRps->Numero = $std->NumeroNf; //ja h� a tag do enrece�o com o nome "Numero", trocado aqui para NumeroNf
		$Rps->InfRps->IdentificacaoRps->Serie = $std->Serie;
		$Rps->InfRps->IdentificacaoRps->Tipo = $std->Tipo;
		$Rps->InfRps->DataEmissao = $std->DataEmissao;//"2019-01-25T13:00:00";
		
		
		//regra de neg�cio para tratamento da natureza de opera��o
		//observe que o noem das vari�veis do objeto $std s�o os mesmos da sirca011
		if((($std->base_iss + $std->base_retiss) > 0) || $std->outmun_iss == "S"){//TRIBUTADO ISS
			//Move 101 To n_NaturezaOperacao
			$Rps->InfRps->NaturezaOperacao = '101';
			
			if ($std->cod_regtrib != "3"){// ( � Simples Nacional)
				//move 107 To n_NaturezaOperacao
				$Rps->InfRps->NaturezaOperacao = '107';
			}
			
			if ($std->outmun_iss == "S")  {
				//Move 102 To n_NaturezaOperacao
				$Rps->InfRps->NaturezaOperacao = '102';
				
				if ($std->cod_regtrib != "3"){ //( � Simples Nacional)
					//move 108 To n_NaturezaOperacao
					$Rps->InfRps->NaturezaOperacao = '108';
				}
			}
			
			if ($std->retencao_iss > 0) {
			//Move 105 To n_NaturezaOperacao
				if($std->cod_regtrib != "3"){// ( � Simples Nacional) 
					//move 110 To n_NaturezaOperacao
					$Rps->InfRps->NaturezaOperacao = '110';
				}
			}
			
		}else{//NAO TRIBUTADO ISS
			//Move 103 To n_NaturezaOperacao
			$Rps->InfRps->NaturezaOperacao = '103';
			
			// Para nfse emitida para propria filial NatOper=Imune.
			if (trim($std->cgc) == trim($std->cgc_rg_entrega)) {
				//Move 104 To n_NaturezaOperacao
				$Rps->InfRps->NaturezaOperacao = '104';
			}
		}
		
		// //regra de neg�cio para nota espec�fica
		// if($std->ItemListaServico == '1002' &&  !in_array($std->NumeroNf, [93528, 93582, 93792, 93345])){
		// 	$Rps->InfRps->NaturezaOperacao = '105';
		// }else if($std->ItemListaServico == '1002' &&  in_array($std->NumeroNf, [93528, 93582, 93792, 93345])){ // 93528, 93582, 93792, 93345
		// 	$Rps->InfRps->NaturezaOperacao = '101';
		// }
		//	echo $std->NumeroNf." ".$Rps->InfRps->NaturezaOperacao." Filial:".$std->FilialCodigoMunicipio." ".$std->CodigoMunicipio;
		//$Rps->InfRps->NaturezaOperacao = $std->NaturezaOperacao;
		if($std->ItemListaServico == '1002' && doubleval($std->IssRetido) == 0){
			$Rps->InfRps->NaturezaOperacao = '101';
		}else if($std->ItemListaServico == '1002' && doubleval($std->IssRetido) > 0){
			$Rps->InfRps->NaturezaOperacao = '105';
		}
		
		$Rps->InfRps->OptanteSimplesNacional = $std->OptanteSimplesNacional;
		$Rps->InfRps->IncentivadorCultural = $std->IncentivadorCultural;
		$Rps->InfRps->Status = $std->Status;
		$Rps->InfRps->Servico = new stdclass();
		$Rps->InfRps->Servico->Valores = new stdclass();
		$Rps->InfRps->Servico->Valores->ValorServicos = $std->ValorServicos;
		
		if(doubleval($std->ValorDeducoes) > 0 ){
			$Rps->InfRps->Servico->Valores->ValorDeducoes = $std->ValorDeducoes;
		}
		
		if(doubleval($std->ValorPis) > 0){
			$Rps->InfRps->Servico->Valores->ValorPis = $std->ValorPis;
		}
		
		if(doubleval($std->ValorCofins) > 0){
			$Rps->InfRps->Servico->Valores->ValorCofins = $std->ValorCofins;
		}
		
		if(doubleval($std->ValorInss) > 0 ){
			$Rps->InfRps->Servico->Valores->ValorInss = $std->ValorInss;
		}
		
		if(doubleval($std->ValorIr) > 0){
			$Rps->InfRps->Servico->Valores->ValorIr = $std->ValorIr;
		}
		
		if(doubleval($std->ValorCsll) > 0){
			$Rps->InfRps->Servico->Valores->ValorCsll = $std->ValorCsll;
		}
		
		$Rps->InfRps->Servico->Valores->IssRetido = (doubleval($std->IssRetido) > 0) ? 1 : 2;
		
		if(doubleval($std->ValorIss) > 0){
			$Rps->InfRps->Servico->Valores->ValorIss = $std->ValorIss;
		}
		
		if(doubleval($std->OutrasRetencoes) > 0 ){
			$Rps->InfRps->Servico->Valores->OutrasRetencoes = $std->OutrasRetencoes;
		}
		
		$Rps->InfRps->Servico->Valores->Aliquota = (!empty($std->Aliquota)) ? ($std->Aliquota / 100) : 0;
		
		if(doubleval($std->DescontoIncondicionado) > 0){
			$Rps->InfRps->Servico->Valores->DescontoIncondicionado = $std->DescontoIncondicionado;
		}
		
		if(doubleval($std->DescontoCondicionado) > 0){
			$Rps->InfRps->Servico->Valores->DescontoCondicionado = $std->DescontoCondicionado;
		}
		
		$Rps->InfRps->Servico->ItemListaServico = $std->ItemListaServico;
		$Rps->InfRps->Servico->Discriminacao = $std->Discriminacao;
		$Rps->InfRps->Servico->CodigoMunicipio = $std->FilialCodigoMunicipio;
		$Rps->InfRps->Servico->CodigoPais = $std->CodigoPais;
		$Rps->InfRps->Prestador = new stdclass();
		//preg_replace utilziado para remover pontos e tra�os dos campos, deixando somente os n�emros
		$Rps->InfRps->Prestador->Cnpj = preg_replace('/[^0-9]/', '', $std->Cnpj);
		$Rps->InfRps->Prestador->InscricaoMunicipal = $std->InscricaoMunicipal;
		$Rps->InfRps->Tomador = new stdclass();
		$Rps->InfRps->Tomador->IdentificacaoTomador = new stdclass();
		$Rps->InfRps->Tomador->IdentificacaoTomador->CpfCnpj = new stdclass();
		
		
		if(strlen(ltrim(preg_replace('/[^0-9]/', '', $std->CpfCnpj))) == 11){

			$Rps->InfRps->Tomador->IdentificacaoTomador->CpfCnpj->Cpf = preg_replace('/[^0-9]/', '', $std->CpfCnpj);
		}else{

			$Rps->InfRps->Tomador->IdentificacaoTomador->CpfCnpj->Cnpj = preg_replace('/[^0-9]/', '', $std->CpfCnpj);
		}

		if (trim($std->cgc) == trim($std->cgc_rg_entrega)) {
			
			$Rps->InfRps->Tomador->IdentificacaoTomador->CpfCnpj->Cnpj = '00000000000000';
		}

		$Rps->InfRps->Tomador->RazaoSocial = $std->RazaoSocial;
		$Rps->InfRps->Tomador->Endereco = new stdclass();
		$Rps->InfRps->Tomador->Endereco->Endereco = $std->Endereco;
		$Rps->InfRps->Tomador->Endereco->Numero = (!empty(trim(preg_replace('/[^0-9]/', '', $std->Numero)))) ? trim(preg_replace('/[^0-9]/', '', $std->Numero)): "s/n";
		$Rps->InfRps->Tomador->Endereco->Complemento = (!empty(trim($std->Complemento))) ? $std->Complemento: "s/n";
		$Rps->InfRps->Tomador->Endereco->Bairro = $std->Bairro;

		$Rps->InfRps->Tomador->Endereco->CodigoMunicipio = $std->CodigoMunicipio;
		//verifica se o CEP esta vazio no RPS
		if(!empty($std->Cep)){

			//Faz validacao do CEP utilizando API REST viacep.com.br
			$dados = $this->procuraPorCEP($std->Cep);

			//Se nao achou o CEP na base de dados
			if(empty($dados->cep)){

				//$std->Cep = '';
				array_push($this->erroCepIncorretos, "CEP INVALIDO  : - ".$Rps->InfRps->IdentificacaoRps->Numero);
			}else{

			
				//Confere o Codigo IBGE retornado com o Cod IBGE da RPS
				if($dados->ibge != ""){
					// Confere se Cod IBGE nota e' igual Cod IBGE ViaCEP
					if($std->CodigoMunicipio != $dados->ibge){
							
							$Rps->InfRps->Tomador->Endereco->CodigoMunicipio = $dados->ibge;
;							array_push($this->erroCepIncorretos, "Cod IBGE  Corrigido Nota : - ".$Rps->InfRps->IdentificacaoRps->Numero);
					}				
				}else{
					array_push($this->erroCepIncorretos, "Não possui Cod IBGE: - ".$Rps->InfRps->IdentificacaoRps->Numero);
				}
			}
			
		//RPS nao possui CEP
		}else{

			//array_push($this->erroCepIncorretos,"Cod CEP - ".$Rps->InfRps->IdentificacaoRps->Numero);
			array_push($this->erroCepIncorretos,"RPS CEP VAZIO ".$Rps->InfRps->IdentificacaoRps->Numero);
			//$std->Cep = '';
					
		}
		
		
		
		$Rps->InfRps->Tomador->Endereco->Uf = $std->Uf;
		$Rps->InfRps->Tomador->Endereco->Cep = preg_replace('/[^0-9]/', '', $std->Cep);

		
		$Rps->InfRps->IntermediarioServico = new stdclass();
		$Rps->InfRps->IntermediarioServico->RazaoSocial = "Consystem Consultoria e Sistemas LTDA";
		$Rps->InfRps->IntermediarioServico->CpfCnpj = new stdclass();
		$Rps->InfRps->IntermediarioServico->CpfCnpj->Cnpj = "95561239000147";
		$Rps->InfRps->IntermediarioServico->InscricaoMunicipal = "2029669";

	
		//pega o nosso objeto $rps j� com as propriedades no padr�o do noe dos campos do xml
		//passa ele para a classe NfeRps que ir� converter um objeto php, para uma string no formato xml
		//classe complexa de ser entendida, verifique sobre recursividade
		$nf = new NfeRps($Rps);

		//retorna a string do xml.
		return $nf->toXml();
	}
	
	/**
	 * newLot
	 * Pega um conjunto de no m�ximo 500 notas e cria um lote
	 * appenda no inicio o cabe�alho do lote e no fim o radap� 
	 *
	 * @param	  $nota, n�array contendo os dados da nota, � uma linha do banco, um resultado do select
	 * @return 	  String, string com o conte�do da nota convertido para xml
	 * @author    Bruno Prece
	 * @since     30/01/2019
	 * @link      Demanda: 42529
	 * @version   1.0
	 */
	function newLot(array $lote, int $filial){
		$numlot  = $this->getLotNumber($filial);
		$filData = $this->getFilInf($filial);
		$cnpj = preg_replace('/[^0-9]/', '', $filData['cgc']);
		$quantidaderps = count($lote); //quantidade de notas no lote passado
		
		$header  = '<?xml version="1.0" encoding="UTF-8" ?>
					<EnviarLoteRpsEnvio xmlns="http://www.publica.inf.br">
<LoteRps versao="1.00">
<NumeroLote>'.$numlot.'</NumeroLote>
<Cnpj>'.$cnpj.'</Cnpj>
<InscricaoMunicipal>'.$filData['insc_municipal'].'</InscricaoMunicipal>
<QuantidadeRps>'.$quantidaderps.'</QuantidadeRps>
<ListaRps>';
		
		//para cada nota do lote, appenda ela no body do lote
		$body = '';
		foreach ($lote as $nota){
			$body.= $nota;
		}
		
		$footer = '</ListaRps>
</LoteRps>
</EnviarLoteRpsEnvio>';
		
		//appenda o header com o body e o footer
		$xml = trim($header).$body.trim($footer);
		
		//manda escrever o arquivo.
		$this->writeFile($xml, $cnpj, $numlot);
	}
	
	/**
	 * formatnf
	 * escreve o xml na home do usu�rio seguindo os padr�es do manual
	 * O nome do arquivo gerado dever� ser formado por: Data no padr�o (AAAAMMDD), o CNPJ do Prestador,
		seguido pelo n�mero do lote (controlado pelo Prestador) com 10 (dez) posi��es, e finalizado pelo sufixo �rps�.
		Exemplo: Lote n�mero 13, gerado no dia 24/12/2011 pelo Prestador de CNPJ 95.836.771/0001-20, dever� ser
		transformado no arquivo XML com o nome 20111224958367710001200000000013rps.xml.
	 *
	 * @param	  $lote, string com o conte�do completo do lote
	 * @param	  $cnpj, cpnj da filial
	 * @param	  numLot, numero do lote que foi capturado do banco, sequence que gera auto�tico por empresa
	 * @return 	  void, nao retorna nada, escreve o xml em um arquivo
	 * @author    Bruno Prece
	 * @since     30/01/2019
	 * @link      Demanda: 42529
	 * @version   1.0
	 * */
	function writeFile(String $lote, $cnpj, $numLot){
		
		//$grupo = ;
		//$empresa = $_SESSION['img_company_ext'];
		//$usuario =  strtolower($_SESSION['log_df_user']);
		
		
		//conversao de pw_db_demo1_demo2 -> demo2
		//$g = substr_replace($grupo,"",-1);
		//$empresa_apelido = str_replace($g."_","",$empresa);

		// $empresa = substr_replace($empresa, "", -1);

		$data = date('Ymd');

		$dir = "/home/consystem/Desktop/";
		$doc = new DOMDocument();
		$doc->loadXML($lote);
		$numLot = str_pad($numLot, 10, "0", STR_PAD_LEFT);
		$doc->save($dir.$data.$cnpj.$numLot."rps.xml");	
		
		$caminho = $dir.$data.$cnpj.$numLot."rps.xml";
		
		array_push($this->path, $caminho);
	}
}
//$conn = new PDO("pgsql:host=devdb.cxzqmawggomt.us-east-1.rds.amazonaws.com;port=5432;dbname=pw_db_demo_demo;user=postgres;password=Cpasscsy-2107") or die('sem conexao');
$conn = new PDO("pgsql:host=nuvemdb.cxzqmawggomt.us-east-1.rds.amazonaws.com;port=5432;dbname=pw_db_sper_sper;user=nuvemdb;password=nuvemdb2017") or die('sem conexao');
$a = new Sircc249($conn,[1], '2018-12');

//print_r( $a->erroCepIncorretos);
$aux =  implode(",",$a->erroCepIncorretos);
$aux = str_replace(",","<br>",$aux);
echo $aux;
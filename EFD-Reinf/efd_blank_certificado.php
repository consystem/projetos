<?
/**
 * Verifica se o certificado pertence aquela empresa comparando
 * a data de validação dentro do certificado com a data atual.
 * retorna a situação do certificado
 *
 ** A CERTIFICADO DEVE SER DA EMPRESA EM QUESTÃO E NÃO DO GRUPO **
 * @author    Nicolas Aoki
 * @since     19/10/2018
 * @link      Demanda: 40044
 * @version   1.0.2
 */


sc_include_library("sys", "dsw", "efdreinf/vendor/nfephp-org/sped-common/bootstrap_certificado.php");
sc_include_library("sys", "dsw", "efdreinf/vendor/nfephp-org/sped-common/src/Certificate.php");

$password='';
$certificate='';
$erros = array();
$content = '';

try{
	sc_lookup(certificado, "SELECT certificado,senha FROM efd_configuracoes" , "pw_db_sx");
	
}catch(Throwable $e){
	array_push($erros,"<br>Consulta ao banco falhou, possivel erro de conexão.");
	//$erros .= "<br>Consulta ao banco falhou, possivel erro de conexao";
	//echo json_encode(["alert" => "Consulta ao banco falhou, possivel erro de conexao"]);
}
if(!empty({certificado[0][0]})){
	
	//verifica se a senha do certificado esta ok
	if(!empty({certificado[0][1]})){
		
		$password = {certificado[0][1]};
		//verifica se o nome do certificado esta ok
		try{
			if(file_exists("../_lib/libraries/sys/dsw/efdreinf/vendor/nfephp-org/sped-efdreinf/src/Factories/".												{certificado[0][0]})){
			$content = file_get_contents("../_lib/libraries/sys/dsw/efdreinf/vendor/nfephp-org/sped-efdreinf/src/Factories/".												{certificado[0][0]});
				
				if(empty($content)){
					array_push($erros,"<br>Erro ao ler certificado. Verifique o nome do arquivo ou diretório.");
				}
			}else{
				array_push($erros,"<br>Erro ao ler certificado. Verifique o nome do arquivo ou diretório.");
			}
			

		}catch(Throwable $e){
			array_push($erros,"<br>Erro ao ler certificado. Verifique o nome do arquivo ou diretório.");
			//$erros .= "<br>Erro ao ler certificado. Verifique o nome do arquivo ou diretorio";
			//echo json_encode(["alert" => "Erro ao ler certificado"]);
		}

	}else{
		array_push($erros,"<br>Erro de acesso. Senha inválida!");
		//$erros .= "<br>Erro de acesso. Senha invalida";
		//echo json_encode(["alert" => "certificado ou senha invalida"]);
	
	}
	try{
		
		$certificate =  NFePHP\Common\Certificate::readPfx($content, $password);
		
	}catch(Throwable $e){
		
		array_push($erros,"<br>Erro ao ler conteudo do certificado A1. Verifique a senha!");
		//$erros .= "<br>Erro ao ler conteudo. Verifique a senha";
		//echo json_encode(["alert" => "certificado ou senha invalida"]);
		
	}
}
//se nao obteve resultados
// certificado inexiste no banco
else{
	array_push($erros,"<br>Certificado não cadastrado!");
	//$erros .= "<br>Certificado inexiste no banco";
	//echo json_encode(["alert" => "Certificado inexiste no banco"]);
}

// Armazenara data limite do arquivo
// e o cnpj contido
$data_limite = '';
$cnpj = '';

//recebe os valores da nota
//$certificate->publicKey
if($certificate){
	
	foreach($certificate->publicKey as $key => $value){
	//	echo $key ."<br>". $value;
		if($key == 'validTo'){

			$data_limite = current($value);

			$data_limite = substr($data_limite,0,10);

		}else if($key == 'cnpj'){

			$cnpj = $value;

		}
	}

}
// Se algum valor estiver vazio
if($data_limite == '' || $cnpj == ''){
	
	array_push($erros,"<br>Campos Data ou CPF inválidos!");
	//$erros .= "<br>Campos Data ou CPF invalidos";
	//echo json_encode(["alert" => "Campos Data ou CPF invalidos"]);
	
}else{

	$cnpj = substr($cnpj,0,8);
	
	sc_lookup(res, "SELECT tpamb FROM efd_configuracoes WHERE emp_nrinsc = '" .$cnpj."'" , "pw_db_sx");
	
	if(!empty({res[0][0]})){
		
		$data_atual = date("Y-m-d");

		if ($data_limite > $data_atual ){
		
			//echo $data_limite . " atual -> " . $data_atual;
			//$erros .= "<br>Dentro do periodo";
			//echo json_encode(["sucess" => "Dentro do periodo"]);
			
		}else{
			array_push($erros,"<br>Certificado vencido!");
			//echo $data_limite . "< " . $data_atual;
			//$erros .= "<br>Certificado vencido";
			//echo json_encode(["alert" => "Certificado vencido: ".$data_limite]);
		}
		
	}else{
		array_push($erros,"<br>CNPJ Não confere!");
		//$erros .= "<br>Certificado inexistente no banco";
		//echo json_encode(["alert" => "Certificado inexistente no banco"]);
	}
	
	
}
if(!empty($erros)){
	echo json_encode(["alert" => $erros[0]]);
}



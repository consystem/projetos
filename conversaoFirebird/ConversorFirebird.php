<?php

Class ConversorFirebird{
	//ID's das respectivas views
	private $views_id = array(
		"gudb_consystem_at_agend_serv"			  =>"concat_ws('-',fll_codigo,atage_id,attpr_id) as id",		
		"gudb_consystem_at_agendamento"			  =>"concat_ws('-',fll_codigo,atage_id) as id",
		"gudb_consystem_at_cso"					  =>"concat_ws('-',fll_codigo,atcso_os,cln_codigo) as id",
		"gudb_consystem_at_cso_itens"			  =>"concat_ws('-',fll_codigo,atcso_osagrupado) as id",
		"gudb_consystem_at_cso_linhas"			  =>"concat_ws('-',fll_codigo,atcso_osagrupado) as id",
		"gudb_consystem_at_mo_executado"		  =>"concat_ws('-',fll_codigo,atcso_os,attpr_id) as id",
		"gudb_consystem_at_mo_recusado"			  =>"concat_ws('-',fll_codigo,atcso_os,attpr_id) as id",
		"gudb_consystem_cgudb_pedidovendainicial" =>"concat_ws('-',fll_codigo,cvpvd_id,cvpvd_numero) as id",
		"gudb_consystem_ep_svc"					  =>"concat_ws('-',fll_codigo,epsvc_pedido,vnd_codigo) as id",
		"gudb_consystem_ep_svi"					  =>"concat_ws('-',fll_codigo,epsvc_pedido,vnd_codigo) as id",
		"gudb_consystem_pi"						  =>"concat_ws('-',fll_codigo,datadelta) as id",
		"gudb_consystem_at_cso_sublet"			  =>"concat_ws('-',fll_codigo, atcso_osagrupado, operationid) as id"
	);
	//Nome das views no Firebird
	public $views_name = array(
		"gudb_consystem_at_agend_serv"			  =>"consystem_at_agend_serv",		
		"gudb_consystem_at_agendamento"			  =>"consystem_at_agendamento",
		"gudb_consystem_at_cso"					  =>"consystem_at_cso",
		"gudb_consystem_at_cso_itens"			  =>"consystem_at_cso_itens",
		"gudb_consystem_at_cso_linhas"			  =>"consystem_at_cso_linhas",
		"gudb_consystem_at_mo_executado" 		  =>"consystem_at_mo_executado",
		"gudb_consystem_at_mo_recusado"			  =>"consystem_at_mo_recusado",
		"gudb_consystem_cgudb_pedidovendainicial" =>"CONSYSTEM_CV_PEDIDOVENDAINICIAL",
		"gudb_consystem_ep_svc"					  =>"consystem_ep_svc",
		"gudb_consystem_ep_svi"					  =>"consystem_ep_svi",
		"gudb_consystem_pi"						  =>"consystem_pi",
		"gudb_consystem_at_cso_sublet"			  =>"consystem_at_cso_sublet"
	);

	//recebe objeto de conexao firebird (iBase)
	public $conFirebird;

	//recebe objeto de conexao PDO
	public $conngp;

	public $conPDO;

	//recebe string de insercao na tabela controle
	public $ultimaSinc;

	//recebe quantidade total de registro a serem atualizados
	public $qtdeRegistro;

	public $empresa;

	public $id;

	public $id_tabela_ford ='';

	function __construct($empresa,$conPDO,$conFirebird,$nome_view=null){

	//function __construct($empresa, $usuario, $conngp,$conFirebird,$nome_view=null){

		$this->empresa = $empresa;
		//$this->conPDO = null;
		$this->conngp = new PDO($conPDO);
		$this->conFirebird = $conFirebird;
		$this->connEmpresa();
		$this->id = getmypid();


		//Se $nome_view estiver vazio
		// executa sincronizacao para todas tabelas
		if(empty($nome_view)){
			foreach ($this->views_id as $view => $id) {
				$this->process(1,$view,'GUDB','SYNC');
				
				$this->insere_tabela_controle($view,$id);
				
				$this->insere_tabela_ford_gudb_evento($view,$this->id_tabela_ford);
				
				$this->process(0,$view,'GUDB','SYNC');
			}
		}else{

			//Se $nome_view estiver alocado
			//executa sincronizacao para determinada tabela
			$this->process(1,$nome_view,'GUDB','SYNC');
			$this->process(3,$nome_view,'GUDB','SYNC',0);
			$this->insere_tabela_controle($nome_view,$this->views_id[$nome_view]);
			if($this->id_tabela_ford)$this->insere_tabela_ford_gudb_evento($nome_view,$this->id_tabela_ford);
			
			$this->process(0,$nome_view,'GUDB','SYNC');
		}
		
	}

	public function connEmpresa(){
//	    $rs_comp = $this->conngp->query("Select db_host ,db_port ,db_user ,db_schema ,db_pass from company where name = '$this->empresa'");
//	$rs_comp = $rs_comp->fetchAll(PDO::FETCH_ASSOC);

    	//$this->conPDO=null;
    	// echo $rs_comp[0]['db_host']."-". $rs_comp[0]['db_port']."-". $rs_comp[0]['db_schema']."-". $rs_comp[0]['db_user']."-".$rs_comp[0]['db_pass'];
    	// echo "\n\n\n\n";
    	//$this->$conPDO = new PDO($rs_comp[0]['db_host'], $rs_comp[0]['db_port'], $rs_comp[0]['db_schema'], $rs_comp[0]['db_user'],$rs_comp[0]['db_pass']);
		
    	//print_r($rs_comp);
    //	echo "pgsql:host=".$rs_comp[0]['db_host']." dbname=".$rs_comp[0]['db_schema']." user=".$rs_comp[0]['db_user']." password=".$rs_comp[0]['db_pass']." ";
//		$this->conPDO = new PDO("pgsql:host=".$rs_comp[0]['db_host']." dbname=".$rs_comp[0]['db_schema']." user=".$rs_comp[0]['db_user']." password=".$rs_comp[0]['db_pass']." ") or die('Conexao ao pgsql incorreta');
	  $arrayPDO = array(
                "host" => "devdb.cxzqmawggomt.us-east-1.rds.amazonaws.com",
                "dbname" => "pw_db_demo_csmt",
                "user" => "postgres",
                "password" => "Cpasscsy-2107"
        );
		 $this->conPDO= new PDO("pgsql:host=".$arrayPDO['host'].";
                                   dbname=".$arrayPDO['dbname'].";
                                   user=".$arrayPDO['user'].";
                                   password=".$arrayPDO['password'])
                                   or die('Conexao ao pgsql incorreta');
	}
	public function insere_tabela_ford_gudb_evento($nome_view,$id){
echo "\n-------Inserindo Tabela Ford: $nome_view-------------\n";
		$sql = "insert into FORD_GUDB_EVENTO (ORIGEM,CHAVE,DATAHORA)VALUES('$nome_view','$id',(select cast('now' as time) from rdb\$database));";
		$query = ibase_prepare($this->conFirebird, $sql);
		$a=ibase_execute($query);
	}
	public function insere_tabela_controle($nome_view,$id){
echo "\n-------Inserindo Tabela Controle: $nome_view-----------\n";
		try{
			

			$controle = array();
			$view = array();
			//Seleciona os registros das tabelas inseridos na tabela controle_firebird
			$stmt = $this->conPDO->query("SELECT id FROM controle_firebird WHERE nome_view = '$nome_view'");
			if(!$stmt){
				echo "Execute query error, because: ". print_r($this->conPDO->errorInfo(),true);
			}
			//insere estes registros num array
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				array_push($controle, $row['id']);
			}
			//Seleciona todos registros da tabela original
			$stmt = $this->conPDO->query("SELECT $id FROM $nome_view");
			
			//insere estes registros num array
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				array_push($view, $row['id']);
			}
			echo "\nControle :" .sizeof($controle). " - $nome_view : " .sizeof($view)."\n";

			//Compara os arrays pegando a diferenca entre eles
			$id_desatualizado = array_merge(array_diff($view,$controle), array_diff($controle,$view));
			$total = sizeof($id_desatualizado);
			//print_r($id_desatualizado);
			if($nome_view == 'gudb_consystem_at_agendamento'){
				unset($id_desatualizado['4-43722']); 
			}
			if($nome_view == 'gudb_consystem_at_cso'){
                                unset($id_desatualizado['1-41568-19241']);
                        }
			if($nome_view =='gudb_consystem_at_mo_executado'){
                                 unset($id_desatualizado["4-36242-9999GRP'U'"]);
				 unset($id_desatualizado["9999GRP'U'"]);
                        }
			if($nome_view == 'gudb_consystem_at_agend_serv'){
                                unset($id_desatualizado['1-46179-,']);
                        }

	
	//Realiza insercao no banco firebird do cliente 
			//se houver registros a serem atualizado na tabela controle
			if($this->insere_tabela_firebird($id_desatualizado,$nome_view)){

				$cont = 0;
				$values = '';
				$insert =  "INSERT INTO controle_firebird (id,nome_view,data_att) VALUES";
				foreach ($id_desatualizado as $id_array => $id_tabela) {
					//echo "contador : $cont\n";
		$this->id_tabela_ford = $id_tabela;			
					$values .=  "('$id_tabela','$nome_view',now()),";
					//realizar insercao em blocos de 1000 registros
					if($cont==1000){
						$porcentagem = ($cont/$total)*100;
                       				$this->process(3,$view,'GUDB','SYNC',$porcentagem);
						$result = $this->conPDO->query($insert . substr($values,0,-1));	
						$cont = 0;
						$values = '';
						if(!$result){
							echo "Execute query error, because: ". print_r($this->conPDO->errorInfo(),true);
						}
					}
					$cont++;
				}
				//Como sao inseridos em blocos de 1000 registros
				// Alguns deles nao foram inseridos dentro do looping
				// Se $values for true, ainda ha registros a serem inseridos
				if($values){
					$result = $this->conPDO->query($insert . substr($values,0,-1));
					if(!$result){
						echo "Execute query error, because: ". print_r($this->conPDO->errorInfo(),true);
					}
					$this->process(3,$view,'GUDB','SYNC',100);
				}
				
			}else{
				echo "falhou na insercao no firebird";
			}

		}catch(Exception $e){
			echo $e;
		}
	}

	/*
	* Inserção generica no banco firebase 
	* $nome_tabela 
	* $coluna array obtendo nome das colunas
	* $insert SQL de insert ja montado
	*
	* chama funcao que rastreia o progresso das insercoes 
	* 
	*/ 
	public function insere_tabela_firebird($id_desatualizado,$nome_view){
	//realiza insercao no banco com ibase
		try {
			
echo "\n-----------inserindo tabela firebird: $nome_view --------------\n";
			$sql = "SELECT * FROM $nome_view WHERE ".$this->views_id[$nome_view]." = ANY('{".rtrim(implode(",",$id_desatualizado),',') . "}')";	
			$sql = str_replace('as id', '', $sql);	
		//echo $sql;	
			$result = $this->conPDO->query($sql);
			if(!$result){
				echo "Execute query error, because: ". print_r($this->conPDO->errorInfo(),true);
			}
			while($row = $result->fetch(PDO::FETCH_ASSOC)){
				//print_r($row);
//				$sql = str_replace("''", 'NULL', "INSERT INTO ".$this->views_name[$nome_view]." VALUES('".implode("','", $row) . "')");

				foreach ($row as $key => $value) {
					$row[$key] = str_replace("'","",$value);
				}
				$sql = str_replace("''", "NULL", "INSERT INTO ".$this->views_name[$nome_view]." VALUES('".implode("','", $row) . "')");

				//$query = ibase_prepare($this->conFirebird, $sql);
				$a = ibase_query($this->conFirebird,$sql);
				if(!$a)	echo $sql;

				//$a=ibase_execute($query);
			//	ibase_free_result($a);
			}
	
		return true;


		} catch (Throwable $e) {
		//	echo $e;
			return false;
		}
	}

	// /* faz update na tabela controle_firebase_sinc
	// *  da quantidade de registro totais e os que ja foram inseridos
	// *
	// * $tabela nome da tabela
	// * $total quantidade total de registros
	// * $atual quantidade atual de registro ja processados
	// */
	// public function rastreiaProgresso($nome_tabela,$porcentagem){
	// 	//insere no banco PGSQL com PDO
	// 	$sql_controle_sinc = "UPDATE controle_firebird_sinc SET porcentual_concluido = " .$porcentagem . " WHERE nome_view = ?";
		
	// 	//echo $sql_controle_sinc;
	// 	$this->conPDO->prepare($sql_controle_sinc)->execute([$nome_tabela]);

	// 	//$statement->execute($statement);

	// }
	public function process($i,$view, $aplicacao='GUDB' ,$operacao='SYNC',$porcent = null)  {

		if ($i == 1) {

			$sql = "INSERT INTO process_control(process_name, application, id_object, process_status, pid_process, company,\"user\", type_pro, time_waiting) VALUES
			('".$view."', '".$aplicacao."', $this->id, 'p', '$this->id', '$this->empresa','tecinco','".$operacao."', 'now()') ON CONFLICT (id_object, application, company) DO NOTHING";

			$stht = $this->conngp->prepare ( $sql );
			$stht->execute ();

			$this->conngp->exec("UPDATE sxsync_blocos set estado = 'E' where funcao = '".$view."';");

		} else if ($i == 0) {

			$sql = "DELETE FROM process_control WHERE id_object = $this->id and company = '$this->empresa' and  application = '".$aplicacao."';";

			$stht = $this->conngp->prepare ( $sql );
			$stht->execute ();

			$this->conngp->exec("UPDATE sxsync_blocos set estado = 'P', ult_execucao = 'now()' where funcao = '".$view."';");

		} elseif ($i == 2) {

			$sql = "UPDATE process_control SET process_status = 'p', time_waiting = 'now()', pid_process = '$this->id'
					WHERE id_object = $this->id and company = '$this->empresa' and  application = '".$aplicacao."';";

			$stht = $this->conngp->prepare ( $sql);
			$stht->execute ();

		}elseif ($i == 3) {
			$sql = "UPDATE process_control SET percent = $porcent WHERE id_object = $this->id and company = '$this->empresa' and  application = '".$aplicacao."';";

			$stht = $this->conngp->prepare ( $sql);
			$stht->execute ();

		}
	}
}
// if(getopt ("cli")){
// 	$empresa        = $argv [2];
// 	$conPDO         = base64_decode($argv[3]);
// 	$conFirebird    = base64_decode($argv[4]);
// 	$nome_view      = $argv [5];
// 	//id nulo pq é processamento automático
// 	$conversor = new ConversorFirebird ($empresa, $conPDO ,$conFirebird,$nome_view);
// }
// $connFirebird = ibase_connect('172.31.30.132:/var/lib/firebird/data/teste1.fdb','SYSDBA','root');
// $connPDO= "pgsql:host=172.31.61.167;port=5432;dbname=pw_db_demo;user=postgres;password=Cpasscsy-2107";
// $conversor = new ConversorFirebird ('DEMO', $conndo ,$conn,"gudb_consystem_at_cso_itens");

//$conversor->sincTabelas();
//$conversor->sinc_pedidovendainicial();

//$conversor->insere_tabela_controle("gudb_consystem_ep_svi","concat_ws('-',fll_codigo,epsvc_pedido) as id");

?> 
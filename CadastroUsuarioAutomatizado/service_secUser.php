<?
/**
 * Serviço da aplicacao de Cadastro  de usuário #app_secUser de acesso ao sistema
 *
 * @author    Nicolas Aoki
 * @since     08/01/2019
 * @link      Demanda: 42236
 * @version   1.0.0
 */

/**
 * Insere novo usuário na tabela sec_user
 *
 * @param params   Array de dados do usuario
 * @throws Throawble Falha na cnexao com o banco
 * @return bool
 */ 
function insereSecUser($params){
    try{

		$md5 = md5($params['senha']);
		$login = $params['login'];
		$nome = ucfirst($params['login']);
		$email = $params['email'];
		$fotoPerfil = $params['img_perfil'];
		$sql = "INSERT INTO public.sec_users(login, pswd, name, email, active, activation_code, priv_admin, image, last_login, theme, mode, font)VALUES ('{$login}','{$md5}','{$nome}','{$email}','Y','','','{$fotoPerfil}',now(),'blue-blue','','')";
		
		$sqlSearch = "select dsw_user from public.sec_user_company where dsw_user = '".$login."'";
		sc_lookup(log,$sqlSearch,"pw_db");
		if(empty({log}[0][0])){		
			sc_exec_sql($sql, "pw_db");	
			return true;
		}else{
			return false;
		}
	}catch(Throwable $e){
		return false;
    }
}
function insereSecUserUpdate($params){
    try{

		$md5 = md5($params['senha']);
		$login = $params['login'];
		$nome = ucfirst($params['login']);
		$email = $params['email'];
		$fotoPerfil = $params['img_perfil'];
		$sql = "INSERT INTO public.sec_users(login, pswd, name, email, active, activation_code, priv_admin, image, last_login, theme, mode, font)VALUES ('{$login}','{$md5}','{$nome}','{$email}','Y','','','{$fotoPerfil}',now(),'blue-blue','','') 
		ON CONFLICT (login)
			DO UPDATE SET 
				login = sec_users.login,
				pswd = '".$md5."',
				name = '".$nome."',
				email = '".$email."' ";
		
		$nm_select = $sql;
		sc_exec_sql($sql,"pw_db");

		return true;

	}catch(Throwable $e){
		return false;
    }
}
/**
 * Insere novo usuário na tabela sec_user_group
 *
 * @param params   Array de dados do usuario
 * @throws Throawble Falha na cnexao com o banco
 * @return bool
 */ 
function insereSecUserCompany($params){

    try{
		sc_lookup(rs,"SELECT current_database()","pw_db");
		$dbName = {rs}[0][0];
		$dbName = str_replace("pw_db_","",$dbName);
		$grupo = $dbName;
		$user = $params['login'];
		$company = $params['nome_empresa'];
		$so_password = strtolower($company)."dealerPW1"; 
		$so_user =  $user."-".strtolower($company);
		$df_user = strtoupper($params['login']);
		$sql = "INSERT INTO public.sec_user_company(dsw_user, df_user, df_password, so_user, so_password, wp_user, wp_password, company)VALUES('{$user}','{$df_user}','','{$so_user}','{$so_password}','','','{$company}')";
		
		sc_exec_sql($sql, "pw_db");
		$emp = strtolower($company);
		//Cria diretorios para aquele usuario
		createDir($grupo, $emp, $user."-".$emp);
		return true;
	}catch(Throwable $e){
        return false;

    }
}
//Insere tabela sec_user_company
function insereSecUserCompanyUpdate($params){

    try{
		sc_lookup(rs,"SELECT current_database()","pw_db");
		$dbName = {rs}[0][0];
		$dbName = str_replace("pw_db_","",$dbName);
		$grupo = $dbName;
		$user = $params['login'];
		$company = $params['nome_empresa'];
		$so_password = strtolower($company)."dealerPW1"; 
		$so_user =  $user."-".strtolower($company);
		$df_user = strtoupper($params['login']);
		
		$sql = "INSERT INTO public.sec_user_company(dsw_user, df_user, df_password, so_user, so_password, wp_user, wp_password, company)VALUES('{$user}','{$df_user}','','{$so_user}','{$so_password}','','','{$company}')
		ON CONFLICT (dsw_user)
			DO UPDATE SET 
			dsw_user = sec_user_company.dsw_user,
			df_user = '".$df_user."',
			so_user = '".$so_user."',
			company = '".$company."' ";
	
		$this->Db->Execute($sql);
		$emp = strtolower($company);
		//Cria diretorios para aquele usuario
		createDir($grupo, $emp, $user."-".$emp);
		return true;
		
	}catch(Throwable $e){
        return false;

    }
}
function getCurrentDbName(){
	sc_lookup(rs,"SELECT current_database()","pw_db");
	$dbName = {rs}[0][0];
	$dbName = str_replace("pw_db_","",$dbName);
	return $dbName;
}

function createDir($grupo, $emp, $user) {
	$dbName = getCurrentDbName();
	$db = "pw_db_";
	$dir2 = "../_lib/dados/pw_db_" . $dbName. "/pw_db_" .$dbName . "_" . $emp . "/usuarios/";
	$cmdMK = "mkdir " . $dir2 . $user;
	$cmdCH = "chmod -R 0777 " . $dir2 . "*";
	$cmdCHOWN1 = "chown  -R daemon:daemon " . $dir2 . "*";
	$cmd = $cmdMK . " && " . $cmdCH . " && " . $cmdCHOWN1;
	exec($cmd);
	sc_exit(sel);
	
}

/**
 * utilizar : https://dkia.dealerweb.com.br/uServices/?service=filiais_usuario&service_param_login=bWFzdGVy&service_param_emp=REtJQQ%3D%3D
 * Procura as filiais cadastradas na Sirsca017
 * para o usuario
 *
 * @param user  nome do usuario
 * @throws Throawble Falha na cnexao com o banco
 * @return bool
 */ 

if(!empty($_GET['arrayLogins']) && !empty($_GET['empresa']) ){
	$list = base64_decode($_GET['arrayLogins']);
	$emp = base64_decode($_GET['empresa']);
	$loginError = array();
	$logo = "logodealer.png";
	$list = explode(',', $list);
	$totalLogins = sizeof($list);
	$contaLogins = 0;
	$aux = array();
	foreach($list as $l){
		$aux['login'] = $l;
		$aux['senha'] = md5($l."123");
		$aux['email'] = $l."@".strtolower($emp).".com";
		$aux['img_perfil'] = $logo;
		$aux['nome_empresa'] = $emp;
		//se houver logins com error
		//salva no array
		if(insereSecUser($aux)){
			insereSecUserCompany($aux);
		}else{
			array_push($loginError,$aux['login']);
		}
		
	}


	echo json_encode(["success" => "Insercao da lista realizada com sucesso.",
					  "falhas"=> sizeof($loginError)." - Usuários Falhos: ".implode(",",$loginError)
					]);

}

if(!empty($_GET['login_param'])){
	
    $dados = $_GET['login_param'];
	
    try{
		//Se formulario for de editar usuario
		if($dados['editar'] == "editar"){
			if(insereSecUserUpdate($dados) && insereSecUserCompanyUpdate($dados)){
				echo json_encode(["success" => "Update realizado com sucesso"]);
			}else{
				echo json_encode(["success" => "Falha ao atualizar"]);
			}
		//cadastra novo usuario
		}else{
			if(insereSecUser($dados) && insereSecUserCompany($dados)){
				echo json_encode(["success" => "Insercao realizada com sucesso"]);
			}else{
				echo json_encode(["success" => "Usuario já inserido, insira outro"]);
			}
		}
    }catch(Throwable $e){
        echo json_encode(["success" => "Erro : ".$e." "]);
        
    }
    
}
























$nomeGlobal = '';
if(!empty($_GET['nome'])){
	$nomeGlobal = $_GET['nome'];
}
//PARA TESTAR APLICACAO COMO FORMULARIO DE EDICAO
// SETAR user = 'fulano' linha 207
?>
<!--/**
 * Cadastro de usuário de acesso do sistema
 *
 * @author    Nicolas Aoki
 * @since     08/01/2019
 * @link      Demanda: 42236
 * @version   1.0.0
 */
-->

<html lang="en">
    <head>


		<script src="<?php echo sc_url_library('sys', 'dsw', 'jquery/3.2.1/jquery.min.js'); ?>"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <title>Cadastrar usuário</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
		<link rel="stylesheet" href="<?php echo sc_url_library('sys', 'dsw', 'jquery/jquery-confirm-master/css/jquery-confirm.css'); ?>">
<script src="<?php echo sc_url_library('sys', 'dsw', 'jquery/jquery-confirm-master/js/jquery-confirm.js'); ?>"></script>
        
        <style type="text/css">
            .iconUsuarioCheck {
                float: right;
                margin-right: 6px;
                margin-top: -23px;
                position: relative;
                z-index: 2;
                color: green;
            }
            .iconUsuarioExclamation{
                float: right;
                margin-right: 6px;
                margin-top: -23px;
                position: relative;
                z-index: 2;
                color: red;
            }
            .iconUsuarioLoad{
                float: right;
                margin-right: 6px;
                margin-top: -23px;
                position: relative;
                z-index: 2;
                color: blue;
            }
            .iconUsuarioError{
                float: right;
                margin-right: 6px;
                margin-top: -23px;
                position: relative;
                z-index: 2;
                color: red;
            }
            .btn-file {
                position: relative;
                overflow: hidden;
            }
            .btn-file input[type=file] {
                position: absolute;
                top: 0;
                right: 0;
                min-width: 100%;
                min-height: 100%;
                font-size: 100px;
                text-align: right;
                filter: alpha(opacity=0);
                opacity: 0;
                outline: none;
                background: white;
                cursor: inherit;
                display: block;
            }

            #img-upload{
                width: 100%;
            }
            #selecionaEmpresas{
                align-content:center;
            }

	article
	{
		width: 80%;
		margin:auto;
		margin-top:10px;
	}


	.area
	{
		border: 5px dotted #ccc;    
		padding: 50px;    
		text-align: center;    
	}

	.drag
	{
		border: 5px dotted green;            
		background-color: yellow;
	}

	#result ul{
		list-style: none;
		margin-top:20px;
	}

    #result ul li
    {
        border-bottom: 1px solid #ccc;
        margin-bottom: 10px;    
    }

        </style>
    </head>
    <body>
        <div class="container" style="margin-top: 5%;">
            <div class="row">
                <div class="col-sm-4"> </div>
                <div class="col-md-4">
                    <h1 id="titulo" class="text-center text-primary"> Cadastrar novo usuário</h1>
                    <br />
                    <div class="col-sm-12">
                        <ul id="navbarBotoes" class="nav nav-pills" >
						  <li class="" style="width:50%"><a class="btn btn-lg btn-default" data-toggle="tab" href="#home">Unico</a></li>
						  <li class="" style="width:48%"><a class=" btn btn-lg btn-default" data-toggle="tab" href="#menu1">Em lote</a></li>
							
						</ul>
						<br/>
                        <div class="tab-content">
							<!-- Insere usuario unico -->
                            <div id="home" class="tab-pane fade in active"> 
								<div class="form-group">
									<label for="UserName">Nome da empresa:</label>
									<input class="form-control" list=empresas id="inputEmpresa">
									<datalist id=empresas>
									</datalist>
								</div>
                                <div class="form-group">
                                    <label for="UserName">Nome de usuario:</label>
                                    <input type="text" class="form-control" id="nomeUsuario"/>
                                </div>
								<div class="form-group" id="filiaisUsuario" style="visibility:hidden;">
									<label for="UserName">Filiais:</label>
									<input class="form-control" list=filiais id="inputFiliais">
									<datalist id=filiais>
									</datalist>
								</div>
                                <div class="form-group">
                                    <label for="email">Endereço de email:</label>
                                    <input type="email" class="form-control" id="email"/>
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Senha:</label>
                                    <input type="password" class="form-control is-invalid" id="pwd1"/>
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Confirmar senha:</label>
                                    <input type="password" class="form-control" id="pwd2"/>
                                </div>
                                <br>
                                <div id="BtnEnviaForm" class="pull-right btn btn-success ">Salvar</div>
                            </div>
							<!-- Insere usuario em lote -->
							<div id="menu1" class="tab-pane fade">
								<div class="form-group">
									<label for="UserName">Nome da empresa:</label>
									<input class="form-control" list=empresas id="inputEmpresa2">
									<datalist id=empresas>
									</datalist>
								</div>
								<div class="form-group">
									<label for="exampleFormControlTextarea1">Insira os logins: </label>
									<div class="alert alert-danger" role="alert">
									  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
									  <span class="sr-only"></span>
									  Atenção ! Inserir na formatação correta.
									</div>
									<textarea maxlength='2000' class="form-control shadow-textarea" id="arrayLogins" rows="5" placeholder="fulano1,fulano2,...,fulano49"></textarea>
							    </div>
								<br/>
								<div id="BtnEnviaLogins" class="pull-right btn btn-success ">Salvar</div>
							</div>

                        </div>
                    </div>
                </div>
			</div>
		<script>   
			
		J = $.noConflict();
		J('#filiaisUsuario').slideUp();
		let form = "cadastrar";
		let user = '<?php echo $nomeGlobal; ?>'
		if(user.length !== 0){
			form = "editar";
			J('#navbarBotoes').hide();
			J('#titulo').text('Editar Usuario');
			J('#nomeUsuario').val(user);
			
		}

		
		</script>
        <script>
			
		J('select').selectpicker();
		
		J(window).on('load', function(){
			J.ajax({
				url: "../uServices",
				type: "GET",
				data: {"service": "company"} ,
				dataType: "JSON",
				success:function(data){	
					J('#inputEmpresa').val(data.company[0]);
					J('#inputEmpresa2').val(data.company[0]);
					J(data.company).each(function(ind,val){
						J('#empresas').append("<option value="+val+"></option>");
					})
				}
			});
            J("#nomeUsuario").after("<span class='fas fa-exclamation iconUsuarioExclamation' id='spinner'></span>");
			
			
			J('#BtnEnviaLogins').on('click', function(){
				let arrayLogins = btoa(J("#arrayLogins").val());
				let empresa = btoa(J("#inputEmpresa2").val());
				if(arrayLogins.length != 0){
					J.ajax({
						url: "../service_secUser",
						type: "GET",
						data: {"arrayLogins": arrayLogins,"empresa":empresa} ,
						dataType: "JSON",
						success:function(data){	
							J.alert({
								confirmButtonClass: 'btn-info',
								title: data.success,
								type: 'green',
								content: data.falhas,
								confirm: function(){
									alert('Confirmed!');
								}
							});
						}
					});
				}
			 })
            J('#BtnEnviaForm').on('click', function () {
				let usuario = J("#nomeUsuario").val();
				let empresa = J('#inputEmpresa').val();
				let mail = J('#email').val();
				let pwd1 = J('#pwd1').val();
				let pwd2 = J('#pwd2').val();
				let editar = form;
                let dados =  {
                                login: usuario,
                                senha: pwd1,
								email: mail,
                                img_perfil:"logodealer.png",
                                nome_empresa: empresa,
								editar: editar
                            };
				if(pwd1 != pwd2){
					J.alert({
						confirmButtonClass: 'btn-info',
						title: 'Verifique sua senha!',
						content: 'os campos devem ser iguais.',
						type: 'red',
						confirm: function(){
							alert('Confirmed!');
						}
					});
					J("#pwd1,#pwd2").css("box-shadow", "2px 2px red");

				}else if(!usuario.length || !empresa.length || !mail.length || !pwd1.length || !pwd1.length || !pwd2.length){
					J.alert({
						confirmButtonClass: 'btn-info',
						title: 'Todos campos devem ser preenchidos !',
						content: ' ',
						type: 'red',
						confirm: function(){
							alert('Ok');
						}
					});
				}else if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))){
					J.alert({
						confirmButtonClass: 'btn-info',
						title: 'Preencha um email válido',
						content: ' ',
						type: 'red',
						confirm: function(){
							alert('Ok');
						}
					});
					J("#email").css("box-shadow", "2px 2px red");
				}else if(J('#spinner').hasClass('iconUsuarioError')){
					J.alert({
						confirmButtonClass: 'btn-info',
						title: 'Insira um usuario valido',
						content: ' ',
						type: 'red',
						confirm: function(){
							alert('Ok');
						}
					});
					J("#email").css("box-shadow", "2px 2px red");
				}else{
					//envia dados do formulario para /service_secUser
					//com todos campos ja tratados
					J.ajax({
						url: "../service_secUser",
						type: "GET",
						data: {"login_param": dados },
						dataType: "JSON",
						success:function(data){
							J("#email,#pwd1,#pwd2").css("box-shadow", "none");
							J("#email,#pwd1,#pwd2,#nomeUsuario").val('');
							usuario = '';
							pwd1 = '';
							pwd2 = '';
							J.alert({
								confirmButtonClass: 'btn-success',
								title: data.success,
								content: ' ',
								confirm: function(){
									alert('Confirmed!');
								}
							});
						}
					});
				}
            });
			
			// metodo que verifica a cada tecla digitada
			// envia-se uma requisicao para uServices metodo filiais_usuario_adaptado
			// retornando as filiais daquele usuario - se tiver
			J('#nomeUsuario').bind("keyup focusin focusout",function(){
				J("#spinner").removeClass();
				J('#spinner').addClass('fas fa-spinner fa-pulse iconUsuarioLoad');
				let usuario = J("#nomeUsuario").val();
				let empresa = J('#inputEmpresa').val();
				usuario = usuario.toUpperCase();
				usuario = btoa(usuario); //converte para base64
				empresa = btoa(empresa);
				let arrayFilial = '';
				J.ajax({
					url: "../uServices",
					type: "GET",
					data: {"service": "filiais_usuario_adaptado", "service_param_login": usuario, "service_param_emp": empresa } ,
					dataType: "JSON",
					success:function(data){
						// se houver filiais naquele usuario retorna os dados
						// como <option> do <datalist> de #filiais
						if(data.filiais_usuario_adaptado.length != 0){
//							J('#inputFiliais').val(data.filiais_usuario_adaptado[0]);
							J('#inputFiliais').attr("placeholder", data.filiais_usuario_adaptado[0]);
							
							J('#filiaisUsuario').css('visibility','visible');
							J('#filiaisUsuario').slideDown();
							
							//Cria nova lista de options com novos dados
							arrayFilial = "<datalist id='filiais'>";
							arrayFilial += data.filiais_usuario_adaptado.map(function(item,index){return "<option value="+item+"></option>"});
							arrayFilial += "</datalist>";
							arrayFilial = arrayFilial.replace(/,/g, '');
							J('#filiais').replaceWith(arrayFilial);

							J("#spinner").removeClass();
							J('#spinner').addClass('fas fa-check iconUsuarioCheck');
						}else{
							J('#inputFiliais').removeAttr("placeholder", data.filiais_usuario_adaptado[0]);
							J('#filiaisUsuario').slideUp();
							J('#inputFiliais').val('');
							//J('#filiais').children().replaceWith("<option value=''></option>");
							J("#spinner").removeClass();
							J('#spinner').addClass('fas fa-times iconUsuarioError');
				
						}
					}
				});
			});
		});

			</script>

    </body>

</html>
<?php

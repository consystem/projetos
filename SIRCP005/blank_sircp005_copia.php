<?php
//Prepara informações usadas para construção.
sc_include_library('sys', 'dsw', 'utilitarios/RemoteFileManager.php', true, true);

$sql = "SELECT type, url, complemento from public.sec_endpoint_company WHERE company ='".[log_empresa]."';";
sc_lookup(url_data, $sql);


//print("<pre>".print_r($url_data,true)."</pre>");

$config = array();
foreach($url_data as $temp){
	$config[$temp[0]] = $temp[1].$temp[2];
}


$group = $_SESSION['scriptcase']['glo_banco'];
//print_r($group);
$sqlTenant="select tid, db_host, db_port,db_schema, db_user, db_pass from ctrl_tenant where db_schema ='$group';";
sc_lookup(rs_tenant, $sqlTenant, "pw_ctrl");


//print("<pre>".print_r($rs_tenant,true)."</pre>");
//print_r($rs_tenant);
if(isset($rs_tenant[0][0])){
	
	 $db_tid   = trim($rs_tenant[0][0]); 	
	 $db_host  = trim($rs_tenant[0][1]);
	 $db_port  = trim($rs_tenant[0][2]);
	 $db_schema= trim($rs_tenant[0][3]);
	 $db_user  = trim($rs_tenant[0][4]);
	 $db_pass  = trim($rs_tenant[0][5]);
}

//Cria conexão e busca dados do usuário
$conn = "pgsql:host=$db_host;port=$db_port;dbname=$db_schema;user=$db_user;password=$db_pass";

$sqlCompany = " SELECT c.df_host , c.ftp_port, uc.so_user, uc.so_password, c.db_schema 
				FROM company c join sec_user_company uc
				on (c.name = uc.company) and (uc.df_user = '".[log_df_user]."') and (c.name = '".[log_empresa]."') LIMIT 1";

//print $sqlCompany;

sc_lookup(dados, $sqlCompany, "pw_db");

//print("<pre>".print_r($dados,true)."</pre>");

$sql_servidor = "select \"user\", password from sec_host_config ;";
sc_lookup(local, $sql_servidor, "pw_db");


//Nomeia dados do sql.
$login_remoto  = $dados[0][2];
$password_remoto  = $dados[0][3];
$login_local = base64_decode($local[0][0]);
$password_local = base64_decode($local[0][1]);
$host  = $dados[0][0];
$port  = $dados[0][1];
$schema = $dados[0][4];

$par_local =  array(    'login'	    => $login_local,
						'password'	=> $password_local);
                        
$par_remoto = array(    'host'	    => $host,
						'login'		=> $login_remoto,
						'password'	=> $password_remoto,
						'port'	    => $port);

print_r($par_remoto);
//Upload de arquivos
$arquivoUploaded = '';
if(!empty($_FILES['uploaded_file'])){
	//Se a option value='' o valor scriptcase
	if($_POST['destinoArquivo'] == 'scriptcase'){
		$path = dirname(__FILE__)."/../_lib/dados/$db_schema/$schema/usuarios/".strtolower($login_remoto)."/";
		$path = $path . basename( $_FILES['uploaded_file']['name']);
	//	echo $path;
		if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $path)) {
			$arquivoUploaded = 'success';
		  // echo "O arquivo ".  basename( $_FILES['uploaded_file']['name']). 
		  // " foi enviado com sucesso";
		} else{
			$arquivoUploaded = 'fail';
			// echo "Ops, houve um erro. Por favor tente novamente";
		}
	}else if($_POST['destinoArquivo'] == 'dataflex'){
		//monta script de envio para o arquivo
		echo "sshpass -p '".$par_remoto['password']."' scp ".$_FILES['uploaded_file']['tmp_name']." ".$par_remoto['login']."@".$par_remoto['host'].":/home/".$par_remoto['login']."/".$_FILES['uploaded_file']['name']." ";


		$arquivoUploaded = 'success';

	}else{
		$arquivoUploaded= 'fail';
	}
	
}

if(empty($host)) {
    echo json_encode("failure:'host vazio!'");
	return;
}
//print_r($par_remoto);
$destino = '../../../../dados/'.$db_schema.'/'.$schema.'/usuarios/'.strtolower($login_remoto).'/';
$link_local = ' ../_lib/dados/'.$db_schema.'/'.$schema.'/usuarios/'.strtolower($login_remoto).'/';
$path = "/home/".strtolower($login_remoto."/");
$link = '../_lib/dados/'.$db_schema.'/'.$schema.'/usuarios/'.strtolower($login_remoto).'/';

$manager = new RemoteFileManager($conn, $par_remoto, $par_local);
$arr = explode("/", $path);
$dir_name = end($arr) ?: $arr[count($arr) - 2];


//print $dir_name;
//print_r($path);
//Cria função de listagem 

$list = function($managr, $path) {
	$files = $managr->scanFiles($path);
	return $files;
};
$list_local = function($managr, $link_local) {
	$files_locais = $managr->scanFiles($link_local,1);
	return $files_locais;
};

/* php_file_tree
 * Retorna HTML da visão em arvore de um diretório.
 * 
 * @param $directory, Array associativo do diretório raiz.
 * 
 * @return $code, código html representando os diretórios.
 * @since 26/03/2018
 * @version 1.0.0
 * @author (https://www.abeautifulsite.net/php-file-tree) / Danilo
 */
function php_file_tree($directory) {
	// Generates a valid XHTML list of all directories, sub-directories, and files in $directory
	$code = "";
	$code .= php_file_tree_dir($directory);
	return $code;
}

/* has_sub_folder
 * verdadeiro se contem pelo menos uma subpasta, falso caso contrário.
 * 
 * @param $directory, Array associativo do diretório.
 * 
 * @return $answer, boolean indicando se contem ou não subdiretórios.
 * @since 04/04/2018
 * @version 1.0.0
 * @author Danilo
 */
function has_sub_folder($directory) {
	$answer = false;
	//Diretorios que são considerados como contendo subfolder
	//mesmo não tendo (irão carregar com ajax.)
	$force_dirs = array("Help_e_Procedimento","Dataflex");
	if (in_array($directory["name"], $force_dirs))
		return true;
	foreach($directory["items"] as $this_file ) {    
		if ($this_file["type"] == "folder"){
			$answer = true;
			break;
		}
	}
	return $answer;
}

/* php_file_tree_dir
 * Constroi representação html de um diretório como
 * lista de diretórios em arvore.
 * esta função é a chamada recursiva de php_file_tree.
 * 
 * @param $directory, array do diretório sendo montado.
 * @param $first_call, (não alterar) indica se é a primeira chamada deste método.
 * 
 * @return $php_file_tree, código html representando os diretórios.
 * @since 26/03/2018
 * @version 1.0.0
 * @author (https://www.abeautifulsite.net/php-file-tree) / Danilo
 */
function php_file_tree_dir($directory, $first_call = true) {
	// Recursive function called by php_file_tree() to list directories/files
    $php_file_tree = "<ul name='".$directory["path"]."' ";
    if( $first_call ) { $php_file_tree .= " class=\"php-file-tree fa-ul\""; $first_call = false; }
    $php_file_tree .= ">";
    foreach($directory["items"] as $this_file ) {    
        if( $this_file["type"] == "folder") {
			//parte do pressuposto que pastas são carregadas
			//antes de qualquer arquivo, se existir 1 pasta
			//deverá indicar que contem subpastas.
			$sub_folder = has_sub_folder($this_file) ? 
				"<i class=\"fa-custom-plus fa-plus\"></i>"
				: 
				"<i class=\"pad-left\"></i>";
            // Folder
            $php_file_tree .= "<li class=\"file-tree-dir\">".$sub_folder."<a class=\"menufolder\" name=\"".$this_file["path"]."\" href=\"".$this_file["path"]."\"><i class=\"fa_custom_i fa-folder\"><span class=\"tree-menu-text\">" . htmlspecialchars($this_file["name"]) . "</span></i></a>";
            $php_file_tree .= php_file_tree_dir($this_file, false);
            $php_file_tree .= "</li>";
        }
    }
    $php_file_tree .= "</ul>";
	return $php_file_tree;
}

//Lista Arquivos para exibição.
//aqui os arquivos são todo o diretorio de arquivos e pastas
$file_list = $list($manager, $path, $dir_name);
$remoto = $file_list;
$file_list_locais = $list_local($manager,$link_local,$dir_name);
//echo count($file_list) ."- ". count($file_list_locais);

//Para ter os diretorios juntos 
//$file_list = array_merge($file_list,$file_list_locais);

//$file_list = array_unique($file_list);
//print_r($file_list_locais);

$file_list = array(
    "name" => $dir_name,
    "type" => "folder",
    "path" => $path,
    "items" => $file_list_locais 
);
// print_r($file_list);
$help_folder = array(
    "name" => "Help_e_Procedimento",
    "type" => "folder",
    "path" => "/home/Help_e_Procedimento/",
    "items" => array()
);
$dataflex = array(
    "name" => "Dataflex",
    "type" => "folder",
    "path" => "/home/Dataflex/",
    "items" => $remoto
);

//Cria file-list superior contendo arquivos e help;
$file_list = array(
    "name" => "home",
    "type" => "folder",
    "path" => "/home/",
    "items" => array ($file_list,$dataflex,$help_folder)
);

?>
<!DOCTYPE html>
<html>
<head lang="pt-br">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="Content-Language" content="pt-br">

	<title>Visualiza&ccedil;&atilde;o de Documentos - SIRCP005</title>
	<!--Variaveis carregadas do servidor-->
    <script type="text/javascript">
		//Define valores para bibliotecas externas.
		var wp_url_get_posts, wp_url_get_category_index, wp_source_category, dsw_home, service_file_manger_url;
		wp_url_get_posts = "<?php echo($config["posts_wp"]) ?>";
		wp_url_get_category_index = "<?php echo($config["categoria_index_wp"]) ?>";
		wp_source_category = <?php echo($config["categoria_fonte"]); ?>;
		dsw_home = "../";
		service_file_manger_url = dsw_home + "blank_file_manager/index.php";

		
        //Insere listagem de arquivos já carregadas direto na variavel
        //que será usada pelo file_script.
        //Carregar antes do script de listagem de arquivos!!!
		var response = [<?php echo(json_encode($file_list));?>];
	</script>
    
	<script src="<?php echo sc_url_library('sys', 'dsw', 'FCO/sircp005/jquery.min.js'); ?>"></script>
	<script src="<?php echo sc_url_library('sys', 'dsw', 'FCO/sircp005/jquery-ui.js'); ?>"></script>
	<!--Home » jquery » 3.2.1 
	<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
	-->
	<script src="<?php echo sc_url_library('sys', 'dsw', 'utilitarios/overlay.js'); ?>"></script>
	<script src="<?php echo sc_url_library('sys', 'dsw', 'FCO/sircp005/sircp005.js'); ?>"></script>
	<script src="<?php echo sc_url_library('sys', 'dsw', 'jquery/jquery-confirm/jquery-confirm.min.js'); ?>"></script>
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet prefetch" href="<?php echo sc_url_library('sys', 'dsw', 'bootstrap/3.3.7/css/bootstrap.min.css');?>">
	<link rel="stylesheet prefetch" href="<?php echo sc_url_library('sys', 'dsw', 'font-awesome/4.7.0/css/font-awesome.min.css');?>">
	<link rel="stylesheet prefetch" href="<?php echo sc_url_library('sys', 'dsw', 'FCO/sircp005/sircp005.css');?>">
	<link rel="stylesheet prefetch" href="<?php echo sc_url_library('sys', 'dsw', 'jquery/jquery-confirm/jquery-confirm.min.css');?>">
<style>
input[type='file'] {
  display: none
}
.inputfile + label {
  background-color:#4682B4;
  border-radius: 5px;
  color: #fff;
  cursor: pointer;
  margin: 10px;
  padding: 6px 20px
}
.inputfile:focus + label,
.inputfile + label:hover {
    background-color: #a0d4e4;
}
.inputfile + label {
	cursor: pointer; /* "hand" cursor */
}
.upload_form{

    padding: 6.25rem 1.25rem;
	width:25%;
	margin:auto;
}
#btnUpload{
	display:none;
}
.excluiArquivo{
	z-index:9999;
}
</style>
</head>
<body>

	<form enctype="multipart/form-data" action="?" method="POST">				
		<input type="file" id="uploaded_files" name="uploaded_file" class="inputfile" multiple></input>
		<label for="uploaded_files">Selecione um arquivo</label>
		  <label for="sel1">Escolha o destino</label>
		  <select class="form-control" name="destinoArquivo" id="sel1">
			<option value="scriptcase">Scriptcase</option>
			<option value="dataflex">Dataflex</option>
		  </select>
		<button class="btn-xs btn-info" id="btnUpload" name="Upload">Upload 					<i class="fa fa-upload" aria-hidden="true"></i>
		</button>
	</form>
	<div class="content">
        <!---Cabeçalho--->
        <div id="header" class="">
			<div class="header-content">
                <div class="header-title">
                    <span>
                        Visualiza&ccedil;&atilde;o de Documentos - SIRCP005
                    </span>
                </div>
				<div class="header-wrapper">
					<div class="breadsearch col-md-11">
						<div class="align-bottom">
							<div class="search">
								<input type="search" name="search" id="search" placeholder="Buscar Arquivo...">
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
        <div id="wrapper" class="row">
            <!---Menu Lateral--->
            <div id="sidebar" class="col-md-2 ui-widget-content">
				<?php echo(php_file_tree($file_list)); ?>
            </div>
            <div id="page-wrapper" class="col-md-10">
                <!---Navegador de Arquivos--->
                <div class="filemanager" id="filemanager">	
                    <div class="breadcrumbs">
						<span>BreadCrumbs.</span>
					</div>
                    <ul class="data"></ul>
                    <div class="nothingfound">
                        <div class="nofiles"></div>
                        <span>Diret&oacute;rio Vazio.</span>
                    </div>
                </div>
                <!---Help--->
                <div class="help" id="help">
					<iframe id="frame_help"src="../blank_help/index.php" class="frame_help"></iframe>
                </div>
            </div>
        </div>
    </div>	
	<script>
		$(document).ready(function() {
			//recebe a variavel $arquivoUploaded
			var arquivoUpado = '<?php echo $arquivoUploaded; ?>';
			if(arquivoUpado=='success'){
				$.confirm({
				title: 'Upload realizado com sucesso',
				content: '',
				type: 'green',
				typeAnimated: true,
				buttons: {
					fechar:{
						action: function(){}
						}
					}
				});
			}
		});
		$(".excluiArquivo").on('hover', function(event){
				event.stopPropagation();
				event.stopImmediatePropagation();
				console.log("TESTE");
				//(... rest of your JS code)
			});
		$('#uploaded_files').on('change', function () {
		  	var file = this.files[0];
			//Se o tamanho do arquivo exceder 10mb
			if (file.size > 100000000) {
				//converte de bytes para Mbytes
				var tamanhoArquivo = file.size/1024/1024;
				//Restringe a 2 casas decimais
				tamanhoArquivo = tamanhoArquivo.toFixed(2);
				$.confirm({
				title: 'o arquivo excede o tamanho de 100MB',
				content: 'Nome: '+file.name+'<br>Tamanho:'+tamanhoArquivo+'MB',
				type: 'red',
				typeAnimated: true,
				buttons: {
					fechar: {
						text: 'Tente novamente',
						btnClass: 'btn-red',
						action: function(){}
						}
					}
				});
			  }else{
			  	$.confirm({
					title: 'Pronto para realizar o upload!',
					content: '',
					icon: 'fa fa-upload',
					type: 'blue',
					typeAnimated: true,
					buttons: {
						upload: {
							btnClass: 'btn-info',
							action: function(){
								$('#btnUpload').click();
							}
						},
						fechar:{
							action: function(){}
						}
					}
				});
			  }

		  // Also see .name, .type
		});
	</script>
</body>
</html>

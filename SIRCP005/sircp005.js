//Variaveis externas necessárias para aplicação:
//var wp_url_get_posts, url para requisitar posts
//wp_url_get_category_index, url para requisitar index de categorias
//wp_source_category; ID da categoria fonte de onde carregar todo o help
//response = [] array contendo elementos para montar arvore de arquivos.
//service_file_manger_url, contendo url do file_manager.

var highlighted_element = null;
var func_search_file;
/**
 * Chama a função carrega_arquivo do help,
 * <Função para evento keydown>
 * @param {object} event - dados do evento.
 */
function func_search_help(event) {
    let enter_code = 13;
    let pressed_enter = (event.keyCode == enter_code);
    if (pressed_enter) {
        $("#frame_help").get(0).contentWindow.carrega_arquivo($("#search").val());
    }
}

/**
 * highlight
 *
 * @param {object}, elemento para destacar.
 * Também exibe todos objetos pais até o elemento
 * por meio de slideDown para que o mesmo fique visivel.
 */
function highlight(element) {
    let cls_highlight = "highlight";
    if (highlighted_element) {
        $(highlighted_element).removeClass(cls_highlight);
    }
    $(element).addClass(cls_highlight);
    $(element).parents("ul").slideDown("medium");
    highlighted_element = element;      
}

/**
 * Evento que faz o slide no menu lateral.
 * <Função para evento click>
 * @param {object} event - dados do evento.
 */
function file_tree_dir_click(event) {
    $(this).parent().find("UL:first").slideToggle("medium");
        if( $(this).parent().attr('className') == "file-tree-dir" ) return false;
    return false;
}

/**
 * mode_help
 *
 * Altera para modo de ajuda, exibindo help para o usuário.
 * @param {event} opcional, dados de evento caso seja feito clique ou outro tipo de ação similar..
 */
function mode_help(e) {
    if(e) {
        e.preventDefault();
    } 
    //Altera exibição
    $("#filemanager").hide();
    $("#help").show();
    $("#tag-li").show();
    $("#categ-li").show();
    //Altera para modo de navegação em caso de pesquisa
    $("#search").on("click.browse", mode_browse);
    $("#search").val("");
    $(".breadcrumbs").text("");
}

/**
 * mode_browse
 *
 * Altera para modo de navegação, exibindo arquivos / nome de arquivos de ajuda para o usuário.
 * @param {event} opcional, dados de evento caso seja feito clique ou outro tipo de ação similar..
 */
function mode_browse(e) {
    //Altera exibição
    $("#filemanager").show();
    $("#help").hide();
    $("#tag-li").hide();
    $("#categ-li").hide();
    $("#search").off("click.browse");
    $("#search").val("");
    return false;
}

/**
 * Carrega subcategorias para um objeto de fonte.
 * - feito por requisição ajax -
 * @param {int} source_id - categoria pai, ex: 1310.
 * @param {object} source_obj, objeto contendo campo items do tipo array, onde será carregada
 * a sub-estrutura a partir da categoria source_id.
 * @param {string} source_selector, seletor jquery para elemento source, usado para poder apendar html.
 * apenda em itens 
 * {
 *  name: categoria, 
 *  type: "folder", 
 *  path: "path source + /slug da categoria."
 *  description: "description da categoria",
 *  items: array para carregar helps da categoria e sub-categorias.
 * }
 */
function load_categories(source_id, source_obj, source_selector) {
    let url = wp_url_get_category_index + "?parent=" + source_id;
    $.ajax(url).done(function(result) {
        $.each(result.categories, function(index, categ) {
            category = {
                name: categ.slug,
                type: "folder",
                path: source_obj.path + categ.slug + "/",
                description: categ.description,
                items: []
            };
            source_obj.items.push(category);
            let loaded_id = categ.id;
            load_categories_helper(source_id, source_obj, source_selector, loaded_id, category);
        });
    });
}

/**
 * 
 * Método auxiliar que verifica se existem subcategorias para
 * adicionar icone "+" a categoria antes de adicionar categorias filhas.
 * - feito por requisição ajax -
 * @param {int} source_id - categoria pai, ex: 1310.
 * @param {object} source_obj, objeto contendo campo items do tipo array, onde será carregada
 * a sub-estrutura a partir da categoria source_id.
 * @param {string} source_selector, seletor jquery para elemento source, usado para poder apendar html.
 * apenda em itens 
 * @param {int} category_id, id de uma sub-categoria carregada.
 * @category {object}, objeto da categoria carregado, contendo name,type, path, description, items.
 *
 */
function load_categories_helper(source_id, source_obj, source_selector, category_id, category) {
    let url = wp_url_get_category_index + "?parent=" + category_id;
    $.ajax(url).done(function(result) {
        let  sub_folder = "";
        if (result.count != 0) {
            sub_folder = "<i class=\"fa-custom-plus fa-plus\"></i>";
        } else {
            sub_folder = "<i class=\"pad-left\"></i>";
        }
        let element_html = `<li class="file-tree-dir">${sub_folder}<a class="menufolder" name="${category.path}" href="${category.path}"><i class="fa_custom_i fa-folder"><span class="tree-menu-text">${category.name}</span></i></a>
<ul name="${category.path}"></ul></li>`
        $(source_selector).append($(element_html));
        $(source_selector + " a[href='" + category.path + "']").click(function(e){
            e.preventDefault();
            //Compatibilidade com exibição de arquivos.
            var nextDir = $(this).attr('href');
            window.location.hash = encodeURIComponent(nextDir);
            //Altera modo para navegação.
            mode_browse();
        $(this).parent().find("UL:first").slideToggle("medium");
            if( $(this).parent().attr('className') == "file-tree-dir" ) return false;
        });
        
        let selector = "ul[name = '" + category.path+ "']";
        load_categories(category_id, category, selector);
        load_helps(category_id, category);
    });
}


/**
 * Carrega helps que contenham categoria category_id em source_obj
 * - feito por requisição ajax -
 * @param {int} source_id - categoria pai, ex: 1310.
 * @param {object} source_obj, objeto contendo campo items do tipo array, onde será carregado
 * os helps percententes a categoria.
 * apenda em itens 
 * {
 *  name: slug do post, 
 *  type: "folder", 
 *  path: "path source + /slug do post."
 *  description: "titulo do post após '-' ",
 *  size: "", vazio.
 * }
 * @param {string} source_selector, seletor jquery para elemento source, usado para poder apendar html.
 */
function load_helps(category_id, source_obj, source_selector) {
    load_helps_segment(category_id, source_obj, source_selector, 1);
}

/**
 * Função auxiliar que carrega os helps em lotes de 50.
 * - feito por requisição ajax -
 * @param {int} category_id - categoria do qual irá carregar os helps.
 * @param {object} source_obj, objeto contendo campo items do tipo array, onde será carregado
 * os helps percententes a categoria.
 * apenda em itens 
 * {
 *  name: slug do post, 
 *  type: "folder", 
 *  path: "path source + /slug do post."
 *  description: "titulo do post após '-' ",
 *  size: "", vazio.
 * }
 * @param {string} source_selector, seletor jquery para elemento source, usado para poder apendar html.
 * @param {int} page, numero da página para requisição
 */
function load_helps_segment(category_id, source_obj, source_selector, page) {
    let url = wp_url_get_posts;
    let posts_amount = 50;
    url += "?post_type=help&orderby=slug&order=ASC&posts_per_page=" + posts_amount + "&category__in=" + category_id
        + "&page=" + page;
    $.ajax(url).done(function(result) {
        
        $.each(result.posts, function( index, post ){
            let separator = "&#8211; ";
            let has_separator = post.title.indexOf(separator) != -1;
            if (!has_separator)
                separator = "";
            let desc = post.title.substring(post.title.indexOf(separator) + separator.length);
            help_item = { 
                name: post.slug, 
                type: "help",
                path: source_obj.path + post.slug,
                description: desc,
                size: ""
            };
            source_obj.items.push(help_item);
       });
        
       //Solicita páginas restantes.
       let has_pages_left = result.count >= posts_amount
       if (has_pages_left) {
           load_helps_segment(category_id, source_obj, source_selector, page + 1);
       }
        
    }).fail(function(jqXHR, textStatus){/*
        if (window.confirm("Falha ao carregar Helps.\nTentar Novamente?")) {
            load_helps_segment(category_id, source_obj, source_selector, page);
        }*/
    });
    
}

/**
 * Envia requisição para download de um arquivo.
 * @param {string} file_path - local remoto do arquivo, ex: /home/danilo/relatorio.txt
 */
function download_file(file_path) {
    'use strict';
    var post_data, callback;
    post_data = { operation : "download", path : file_path};
    callback = function (url) {
        var window_url;
        if(url.indexOf("failure:'host vazio!(FileManager)") !== -1){
            alert("Erro ao baixar o arquivo!");
            return;
        }
        window_url = window.location.pathname;
        window_url = window_url.substr(0, window_url.lastIndexOf("/") + 1);
        window_url += url;
        window.open(window_url);
    };
    $.post(service_file_manger_url, post_data, callback);
}


$(document).ready( function() {
    
    //Requisita carregamento das categorias de help.
    load_categories(wp_source_category, response[0].items[1], "ul[name='/home/Help_e_Procedimento/']");
    
    //Configura sidebar para ser expansivel.(requere jquery.ui)
    $("#sidebar" ).resizable({
        maxWidth: 600,
		minWidth: 225,
		handles: "e"
    });
    
    //Esconde sistema de arquivos ao carregar.
    $(".php-file-tree").find("UL").hide();

    // Expande / retrai arvore ao clicar.
    $(".file-tree-dir A").click(file_tree_dir_click);
    
    //Configura overlay
    init_overlay_message("Carregando...");
    
    
    /**
     * adjust_element_class_by
     * 
     * Ajusta classe do elemento, classe deve conter valor númerico no final 
     * de sua declaração, possuir diferenças para cada valor númerico.
     *
     * @param {int} value - quantia de ajuste, valores negativos diminuem o valor.
     * @param {object} element - elemento no qual será alterado o valor da classe.
     * @param {string} class_base_name - nome da classe que será alterada. 
     *
     * Observação, se o elemento tiver mais de uma classe, elas serão removidas.
     *
     * Exemplo: elemento page-wrapper possui classe col-md-5, para alterar
     * para a classe col-md-2 pode ser utilizado:
     * adjust_element_class_by(-3, $("#page-wrapper"), "col-md-");
     */
    function adjust_element_class_by(value, element, class_base_name) {
        let element_class = $(element).attr("class");
        let numbers = element_class.match(/\d+/g).map(Number);
        let previous_value = numbers[0];
        let new_class = class_base_name + (previous_value + value);
        $(element).removeClass(element_class);
        $(element).addClass(new_class);
    }
    
    
    /**
     * Evento que ajusta tamanho do page-wrapper ao ocorrer resize.
     */
    $("#sidebar").resize(function(e) {
        let resize_amount = 1;
        let base_class = "col-md-";
        let screen_width = window.innerWidth;
        let resized_width = $("#sidebar").outerWidth() + $("#page-wrapper").outerWidth();
        if (resized_width >= screen_width) {
            adjust_element_class_by(-resize_amount, $("#page-wrapper"), base_class);
        } else {
            let col_size_percentage = 0.0834;
            let has_space = (screen_width - resized_width) > screen_width * col_size_percentage; 
            if (has_space) {
                adjust_element_class_by(resize_amount, $("#page-wrapper"), base_class);
            }
        }
    });
    
    //Fecha overlays ao receber informação.
    $($("#frame_help")[0].contentWindow.window).on('load', function() { 
        $("#frame_help")[0].contentWindow.ajax_callback = overlay_hide;
        $("#frame_help")[0].contentWindow.start_listening();
        
        //Altera forma de chamada de helps para exibir overlay.
        var old_carrega_arquivo = $("#frame_help")[0].contentWindow.carrega_arquivo;
        $("#frame_help")[0].contentWindow.carrega_arquivo = function (arq) {
            overlay_show();
            old_carrega_arquivo(arq);
        }
    });
    
});	

//Exibe Help ou Arquivos conforme clique em menus laterais
$(function() {
    
    $("#help").hide();

    /**
     * Exibe o help e oculta o navegador de arquivos.
     * @param {object} e - dados do evento de clique.
     */
    $("a[name='abreHelp'").on("click", mode_help);
    /**
     * Exibe navegador de arquivos e oculta o help.
     * @param {object} e - dados do evento de clique.
     */
    $(".menufolder").on("click", mode_browse);
    
    //Variaveis para exibição e pesquisa de arquivos.
    var filemanager = $('.filemanager'),
	breadcrumbs = $('.breadcrumbs'),
    searchbox = $('#search'),
    menufolder = $('.menufolder')
	fileList = filemanager.find('.data');
    //service_file_manger_url, variavel configurada na aplicação.
    var currentPath = '',
    breadcrumbsUrls = [],
    data = response[0];

    var folders = [],
    files = [];

    /**
     * Evento para Pesquisa de arquivos
     * @param {string} e - Dados do evento.
     */
    function search_files(e) {

        folders = [];
        files = [];

        var value = this.value.trim();

        if (value.length)// Update the hash on every key stroke
            window.location.hash = 'search=' + value.trim();
        else 
            window.location.hash = encodeURIComponent(currentPath);
    } 
    //Guarda função para ser usada por outros scripts.
    func_search_file = search_files;

    /**
     * Evento ao sair de foco da pesquisa.
     */
    function search_files_focus_out(e) {
        var search = $(this);
        if(!search.val().trim().length) {
            window.location.hash = encodeURIComponent(currentPath);
            search.parent().find('span').show();
        }
    }

    

    // This event listener monitors changes on the URL. We use it to
    // capture back/forward navigation in the browser.

    $(window).on('hashchange', function(){

        goto(window.location.hash);

        // We are triggering the event. This will execute 
        // this function on page load, so that we show the correct folder:

    }).trigger('hashchange');

    // Listening for keyboard input on the search field.
    // We are using the "input" event which detects cut and paste
    // in addition to keyboard input.

    searchbox.on('input.search_files', search_files
    ).on('keyup', function(e){

        // Clicking 'ESC' button triggers focusout and cancels the search

        var search = $(this);

        if(e.keyCode == 27) {

            search.trigger('focusout');

        }

    }).on("focusout.search_files", search_files_focus_out);


    // Clicking on folders

    fileList.on('click', 'li.folders', function(e){
        e.preventDefault();

        var nextDir = $(this).find('a.folders').attr('href');

        if(filemanager.hasClass('searching')) {

            // Building the breadcrumbs

            breadcrumbsUrls = generateBreadcrumbs(nextDir);

            filemanager.removeClass('searching');
            filemanager.find('input[type=search]').val('').hide();
            filemanager.find('span').show();
        }
        else {
            breadcrumbsUrls.push(nextDir);
        }

        window.location.hash = encodeURIComponent(nextDir);
        currentPath = nextDir;
    });


    //Clicking on menufolder

    menufolder.on('click', function(e) {
        e.preventDefault();

        var nextDir = $(this).attr('href');

        if(filemanager.hasClass('searching')) {

            // Building the breadcrumbs

            breadcrumbsUrls = generateBreadcrumbs(nextDir);

            // filemanager.removeClass('searching');
            // filemanager.find('input[type=search]').val('').hide();
            // filemanager.find('span').show();
        }
        else {
            breadcrumbsUrls.push(nextDir);
        }

        window.location.hash = encodeURIComponent(nextDir);
        currentPath = nextDir;
        return false;
    });




    // Clicking on breadcrumbs


    breadcrumbs.on('click', 'a', function(e){
        e.preventDefault();

        var index = breadcrumbs.find('a').index($(this)),
            nextDir = breadcrumbsUrls[index];

        breadcrumbsUrls.length = Number(index);

        window.location.hash = encodeURIComponent(nextDir);

    });


    // Navigates to the given hash (path)

    function goto(hash) {

        hash = decodeURIComponent(hash).slice(1).split('=');
        //Constroi seletor e se nao achar, tenta adicionar "/" para encontrar elemento.
        let selector = "a[name='" + hash[0] + "']";
        if($(selector).length == 0)
            selector = $("a[name='" + hash + "/']")
        highlight($(selector).children("i").get(0));

        if (hash.length) {
            var rendered = '';

            // if hash has search in it

            if (hash[0] === 'search') {

                filemanager.addClass('searching');
                rendered = searchData(response, hash[1].toLowerCase());

                if (rendered.length) {
                    currentPath = hash[0];
                    render(rendered);
                }
                else {
                    render(rendered);
                }

            }

            // if hash is some path

            else if (hash[0].trim().length) {

                rendered = searchByPath(hash[0]);

                if (rendered.length) {

                    currentPath = hash[0];
                    breadcrumbsUrls = generateBreadcrumbs(hash[0]);
                    render(rendered);

                }
                else {
                    currentPath = hash[0];
                    breadcrumbsUrls = generateBreadcrumbs(hash[0]);
                    render(rendered);
                }

            }

            // if there is no hash

            else {
                currentPath = data.path;
                breadcrumbsUrls.push(data.path);
                render(searchByPath(data.path));
            }
        }
    }

    // Splits a file path and turns it into clickable breadcrumbs

    function generateBreadcrumbs(nextDir){
        var path = nextDir.split('/').slice(0);
        for(var i=1;i<path.length;i++){
            path[i] = path[i-1]+ '/' +path[i];
        }
        return path;
    }


    // Locates a file by path

    function searchByPath(dir) {
        var path = dir.split('/'),
            demo = response,
            flag = 0;

        for(var i=0;i<path.length;i++){
            for(var j=0;j<demo.length;j++){
                if(demo[j].name === path[i]){
                    flag = 1;
                    demo = demo[j].items;
                    break;
                }
            }
        }

        demo = flag ? demo : [];
        return demo;
    }


    // Recursively search through the file tree

    function searchData(data, searchTerms) {

        data.forEach(function(d){
            if(d.type === 'folder') {

                searchData(d.items,searchTerms);

                if(d.name.toLowerCase().match(searchTerms)) {
                    folders.push(d);
                }
            }
            else if(d.type === 'file') {
                if(d.name.toLowerCase().match(searchTerms)) {
                    files.push(d);
                }
            }
            else if(d.type === 'help') {
                if(d.name.toLowerCase().match(searchTerms)) {
                    files.push(d);
                }
            }
        });
        return {folders: folders, files: files};
    }


    // Render the HTML for the file manager

    function render(data) {

        var scannedFolders = [],
            scannedFiles = [];

        if(Array.isArray(data)) {

            data.forEach(function (d) {

                if (d.type === 'folder') {
                    scannedFolders.push(d);
                }
                else if (d.type === 'file') {
                    scannedFiles.push(d);
                }
                else if (d.type === 'help') {
                    scannedFiles.push(d);
                }

            });

        }
        else if(typeof data === 'object') {

            scannedFolders = data.folders;
            scannedFiles = data.files;

        }


        // Empty the old result and make the new one

        fileList.empty().hide();

        if(!scannedFolders.length && !scannedFiles.length) {
            filemanager.find('.nothingfound').show();
        }
        else {
            filemanager.find('.nothingfound').hide();
        }

        if(scannedFolders.length) {

            scannedFolders.forEach(function(f) {

                var itemsLength = f.items.length,
                    name = escapeHTML(f.name),
                    icon = '<span class="icon folder"></span>';

                if(itemsLength) {
                    icon = '<span class="icon folder full"></span>';
                }

                if(itemsLength == 1) {
                    itemsLength += ' item';
                }
                else if(itemsLength > 1) {
                    itemsLength += ' items';
                }
                else {
                    itemsLength = 'Empty';
                }
                console.log(f.path);
                var folder = $('<li class="folders"><a href="'+ f.path +'" title="'+ f.path +'" class="folders">'+icon+'<span class="name">' + name + '</span> <span class="details">' + itemsLength + '</span></a></li>');
                folder.appendTo(fileList);
            });

        }

        if(scannedFiles.length) {

            scannedFiles.forEach(function(f) {

                var fileSize = f.type == 'file' ? f.size : f.description,
                    name = escapeHTML(f.name),
                    fileType = name.indexOf('.') != -1 ? name.split('.') : " ",
                    icon = '<span class="icon file"></span>';

                fileType = fileType[fileType.length-1];

                icon = '<span class="fa fa-window-close fa-2x excluiArquivo" aria-hidden="true" style="float:right"></span><span class="icon file f-'+fileType+'">.'+fileType+'</span>';
                var file = null;
                if (f.type == 'file') {
                file = $('<li class="files"><a href=# title="'+ f.path +
                             '" class="files">' + icon+'<span class="name">' +
                             name +'</span> <span class="details">' + fileSize +
                             '</span></a></li>').click(function(){download_file(f.path);return false;});
                } 
                else {
                    file = $('<li class="files"><a href=# title="'+ f.path +
                             '" class="files">' + icon+'<span class="name">' +
                             name +'</span> <span class="details">' + fileSize +
                             '</span></a></li>').click(
                        function(){
                            overlay_show();
                            mode_help(); 
                            let help_url = $("#frame_help")[0].contentWindow.helpUrlBase + f.name;
                            $("#frame_help").get(0).contentWindow.lerHelp(help_url);
                            return false;
                        });
                }
                file.appendTo(fileList);
            });

        }

        // Generate the breadcrumbs

        var url = '';

        if(filemanager.hasClass('searching')){

            url = '<span>Resultado da Busca: </span>';
            fileList.removeClass('animated');

        }
        else {

            fileList.addClass('animated');

            breadcrumbsUrls.forEach(function (u, i) {

                var name = u.split('/');

                if (i !== breadcrumbsUrls.length - 1) {
                    url += '<a href="'+u+'"><span class="folderName">' + name[name.length-1] + '</span></a> <span class="arrow">→</span> ';
                }
                else {
                    url += '<span class="folderName">' + name[name.length-1] + '</span>';
                }

            });

        }

        breadcrumbs.text('').append(url);


        // Show the generated elements

        fileList.animate({'display':'inline-block'});

    }

    // This function escapes special html characters in names

    function escapeHTML(text) {
        return text.replace(/\&/g,'&amp;').replace(/\</g,'&lt;').replace(/\>/g,'&gt;');
    }
});
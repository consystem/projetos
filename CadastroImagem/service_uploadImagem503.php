<?
/**
 * Cadastro de imagem de usuario
 * Recebe uma imagem por $_FILE, grava no diretorio 
 * /scriptcase/wwwroot/scriptcase/prod/img/perfil_usuario/
 * e salva no banco na tabela public.sec_img_user
 *
 * @author    Nicolas Aoki
 * @since     29/01/2019
 * @link      Demanda: 41217
 * @version   1.0.0
 */

if(!empty($_GET['cdfilial']) && !empty($_GET['codfunc'])){
	$filial = $_GET['cdfilial'];
	$func = $_GET['codfunc'];
	$path = "/scriptcase/wwwroot/scriptcase/prod/img/perfil_usuario/".[nomeArq];
	$sql = "INSERT INTO public.sec_user_img(id,cdfilial_fk, codfunc_fk, path,historico) VALUES (DEFAULT,'".$filial."','".$func."','".$path."',now())";
	sc_exec_sql($sql, "pw_db");
}

if(isset($_FILES['file']['name'])){
	
	//exec("mkdir /scriptcase/wwwroot/scriptcase/prod/img/perfil_usuario/teste");
	// exec("chmod 777  /scriptcase/wwwroot/scriptcase/prod/img/*");
	//echo getcwd();
	
	$filename = $_FILES['file']['name'];
	//descomentar linha abaixo
	//[nomeArq] = $filename;
	$location = "/scriptcase/wwwroot/scriptcase/prod/img/perfil_usuario/".$filename;
	$display_image = "/scriptcase/prod/img/perfil_usuario/".$filename;
	$uploadOk = 1;
	$imageFileType = pathinfo($location,PATHINFO_EXTENSION);

	exec("chmod 777  /scriptcase/wwwroot/scriptcase/prod/img/perfil_usuario/*");

	$valid_extensions = array("jpg","jpeg","png");
	
	if( !in_array(strtolower($imageFileType),$valid_extensions) ) {
	   $uploadOk = 0;
	}
	
	if($uploadOk == 0){
	   echo json_encode(["success" => $display_image]);
	}else{
	
	   if(move_uploaded_file($_FILES['file']['tmp_name'],$location)){
		   echo json_encode(["success" => $display_image]);
	   }else{
		   echo json_encode(["success" => "Nao foi possivel fazer o upload"]);
	   }
	}
}

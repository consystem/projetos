?>
<!--/**
 * Cadastro de imagem de usuario
 *
 * Caso precise alocar a imagem do usuario dentro do input
 * /https://stackoverflow.com/questions/51737022/how-to-put-input-tag-inside-img-tag
 * https://stackoverflow.com/questions/2855589/replace-input-type-file-by-an-image
 * @author    Nicolas Aoki
 * @since     29/01/2019
 * @link      Demanda: 41217
 * @version   1.0.0
 */
-->
<head>
<link rel="stylesheet" href="<?php echo sc_url_library('sys', 'dsw', 'bootstrap/3.3.7/css/bootstrap.min.css'); ?>">
<script src="<?php echo sc_url_library('sys', 'dsw', 'jquery/3.2.1/jquery.min.js'); ?>"></script>
<link rel="stylesheet" href="<?php echo sc_url_library('sys', 'dsw', 'jquery/jquery-confirm-master/css/jquery-confirm.css');?>">
<script src="<?php echo sc_url_library('sys', 'dsw', 'jquery/jquery-confirm-master/js/jquery-confirm.js'); ?>"></script>
<script src="<?php echo sc_url_library('sys', 'dsw', 'font-awesome/5.0.9/svg-with-js/js/fontawesome-all.min.js'); ?>"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</head>
<style>
/* Container */
.container{
   margin: 0 auto;
   border: 0px solid black;
   width: 50%;
   height: 250px;
   border-radius: 3px;
   background-color: ghostwhite;
   text-align: center;
}
/* Preview */
.preview{
   width: 100px;
   height: 100px;
   border: 1px solid black;
   margin: 0 auto;
   background: white;
}

.preview img{
   display: none;
}
/* Button */
.button{
   border: 0px;
   background-color: deepskyblue;
   color: white;
   padding: 5px 15px;
   margin-left: 10px;
}
.image-upload > input
{
    display: none;
}
</style>
<!-- Formulario de input de imagem -->
<body>
	<div class="container">
    <form method="post" action="" enctype="multipart/form-data" id="myform">
        <div class='preview'>
            <img src="" id="img" width="100" height="100">
        </div>
        <div >
            <input type="file"  id="file" name="file" />
            <input type="button" class="button" value="Upload" id="but_upload">
        </div>
    </form>
</div>
<script>
$(document).ready(function(){
	// MODO DE USO.
	//Alterar os respectivos codigos para 
	// o usuario em questão
	var cdfilial = '321';
	var codfunc = '123';
	
	function insereImagemBanco(cdfilial,codfunc){

		 $.ajax({
			type: "GET",
			url: "../service_uploadImagem503",
			data: {"cdfilial":cdfilial,"codfunc":codfunc},
			dataType: "JSON",
			success: function(r){
				console.log(r);
			}
		});
	}
	
	$(".preview").slideUp();
	//Envio da imagem para gravar no diretorio
    $("#but_upload").click(function(){

        var fd = new FormData();
        var files = $('#file')[0].files[0];
        fd.append('file',files);
		
		//faz envio da imagem
        $.ajax({
            url: '../service_uploadImagem503/index.php',
            type: 'post',
			dataType: "JSON",
            data: fd,
            contentType: false,
            processData: false,
            success: function(response){
				
				//Caso queira mostrar um obj estará contido em response.success
				// o retorno da inserção concluida
				// @return : /caminho/da/imagem;
				console.log(response.success);
				$(".preview").slideDown();
				$("#img").attr("src",response.success); 
				$(".preview img").show(); // Display image element
        	},
			complete:function (){
			  insereImagemBanco(cdfilial,codfunc);
			}
		})
	});
});
</script>
</body>
<?
<?php

/**
 * Verifica se usuário a ser criado
 * está previamente cadastrado pelo dataflex
 * ou seja, constará na tabela sirca017
 *
 * @param user  nome do usuario
 * @throws Throawble Falha na cnexao com o banco
 * @return bool
 */ 
function verificaUsuario($user){

    try{
       if($user == 'master'){
            return true;
        }else{
            return false;
        }
    }catch(Throwable $e){
        echo json_encode(["Error" => "Falha na conexão"]);
    }
}
/**
 * Insere novo usuário na tabela sec_user
 *
 * @param params   Array de dados do usuario
 * @throws Throawble Falha na cnexao com o banco
 * @return bool
 */ 
function insereSecUser($params){
    try{
      /*   if(){

        }else(){
            echo json_encode(["Error" => "Erro na inserção do usuário"]);
        } */
    }catch(Throwable $e){
        echo json_encode(["Error" => "Falha na conexão"]);
    }
}
/**
 * Insere novo usuário na tabela sec_user_group
 *
 * @param params   Array de dados do usuario
 * @throws Throawble Falha na cnexao com o banco
 * @return bool
 */ 
function insereSecUserGroup($params){
    try{

        echo json_encode(["Error" => "Erro na inserção do usuário"]);
    }catch(Throwable $e){
        echo json_encode(["Error" => "Falha na conexão"]);
    }
}

/**
 * utilizar : https://dkia.dealerweb.com.br/uServices/?service=filiais_usuario&service_param_login=bWFzdGVy&service_param_emp=REtJQQ%3D%3D
 * Procura as filiais cadastradas na Sirsca017
 * para o usuario
 *
 * @param user  nome do usuario
 * @throws Throawble Falha na cnexao com o banco
 * @return bool
 */ 
function filiaisUsuario($user){
    try{
       /*  if(){
            return true;
        }else{
            echo json_encode(["Error" => "Nao há filiais para este usuário"]);
        } */
    }catch(Throwable $e){
        echo json_encode(["Error" => "Falha na conexão"]);
    }
}

if(!empty($_GET['login_param'])){
    
    try{
        echo json_encode(["alert" => "Inserido com sucesso"]);

    }catch(Throwable $e){
        echo json_encode(["alert" => "Erro : ".$e." "]);
        
    }
    
}
elseif(!empty($_GET['procuraUsuario'])){
    $usuario = $_GET['procuraUsuario'];

    try{

        if(verificaUsuario($usuario)){
            echo json_encode(["success" => "200"]);
        }else{
            echo json_encode(["alert" => "404"]);
        }
    }catch(Throwable $e){

    }
}else{
    echo json_encode(["alert" => "Argumentos invalidos !"]);
}

<?php
/**
 * Cadastro de usuário de acesso do sistema
 *
 * @author    Nicolas Aoki
 * @since     08/01/2019
 * @link      Demanda: 42236
 * @version   1.0.0
 */

?>
<html lang="en">
    <head>

    <!--     <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script> -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <title>Cadastrar usuário</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
        
        <style type="text/css">
            .iconUsuarioCheck {
                float: right;
                margin-right: 6px;
                margin-top: -23px;
                position: relative;
                z-index: 2;
                color: green;
            }
            .iconUsuarioExclamation{
                float: right;
                margin-right: 6px;
                margin-top: -23px;
                position: relative;
                z-index: 2;
                color: red;
            }
            .iconUsuarioLoad{
                float: right;
                margin-right: 6px;
                margin-top: -23px;
                position: relative;
                z-index: 2;
                color: blue;
            }
            .iconUsuarioError{
                float: right;
                margin-right: 6px;
                margin-top: -23px;
                position: relative;
                z-index: 2;
                color: red;
            }
            .btn-file {
                position: relative;
                overflow: hidden;
            }
            .btn-file input[type=file] {
                position: absolute;
                top: 0;
                right: 0;
                min-width: 100%;
                min-height: 100%;
                font-size: 100px;
                text-align: right;
                filter: alpha(opacity=0);
                opacity: 0;
                outline: none;
                background: white;
                cursor: inherit;
                display: block;
            }

            #img-upload{
                width: 100%;
            }
            #selecionaEmpresas{
                align-content:center;
            }
        </style>
    </head>
    <body>
        <div class="container" style="margin-top: 5%;">
            <div class="row">
                <div class="col-sm-4"> </div>
                <div class="col-md-4">
                    <h1 class="text-center text-primary"> Cadastrar novo usuário</h1>
                    <br />
                    <div class="col-sm-12">
                        <ul class="nav nav-pills" >
                            <li class="" style="width:50%"><a class="btn btn-lg btn-light" data-toggle="tab" href="#home">Converter</a></li>
                            <li class="" style="width:48%"><a class=" btn btn-lg btn-light" data-toggle="tab" href="#menu1">Atrelar Empresa</a></li>
                        </ul>
                        <br/>
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active"> 
                                <div class="form-group">
                                    <label>Foto perfil</label>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <span class="btn btn-default btn-file">
                                                Procurar Imagem<input type="file" id="imgInp">
                                            </span>
                                        </span>
                                        <input type="text" class="form-control" readonly>
                                    </div>
                                    <img id='img-upload'/>
                                </div>
                                <div class="form-group">
                                    <label for="UserName">Nome de usuario:</label>
                                    <input type="text" class="form-control" id="nomeUsuario">
                                </div>
                                <div class="form-group">
                                    <label for="email">Endereço de email:</label>
                                    <input type="email" class="form-control" id="email">
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Senha:</label>
                                    <input type="password" class="form-control" id="pwd">
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Confirmar senha:</label>
                                    <input type="password" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="emp">Selecionar Empresa(s) de acesso:</label>
                                    <select class="selectpicker " multiple data-live-search="true">
                                        <option>Mustard</option>
                                        <option>Ketchup</option>
                                        <option>Relish</option>
                                    </select>
                                </div>
                                <br>
                                <div id="selecionaEmpresas" onclick="writeText()" class="offset-md-5 btn btn-danger">
                                    <span  class='fas fa-arrow-circle-down'></span>
                                </div>
                                
                                <hr>
                                <div class="form-group">
                                    <div id="empresas"></div>
                                </div>
                                <div id="BtnEnviaForm" class="pull-right btn btn-success ">Cadastrar</div>
                            </div>
                                <div id="menu1" class="tab-pane fade">
                                    
                                    <div class="form-group">
                                        <label for="UserName">Nome de usuario:</label>
                                        <input type="text" class="form-control" id="user">
                                    </div>
                                    <div class="form-group">
                                        <label for="pwd">Senha:</label>
                                        <input type="password" class="form-control" id="senha">
                                    </div>
                                    <div class="form-group">
                                        <label for="sel2">Selecione as Empresas:</label>
                                        <select multiple class="form-control" id="sel2">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="comment">Empresas Selecionadas:</label>
                                        <textarea class="form-control" rows="5" id="empresasSelecionadas"></textarea>
                                    </div>
                                    <div id="BtnEnviaForm" class="pull-right btn btn-success ">Cadastrar</div>
                                    
                                    <div id="jaCadastrado" class="pull-left btn btn-warning" > Usuário já cadastrado ?</div>                    
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <script>

        $('select').selectpicker();
        function writeText() {
            var empresas = document.getElementById('comment');
            var selected = $("#emp").val();
            console.log(selected);
            selected = selected.map(function(e){return " <span style='font-size:20px;' class='badge badge-warning'>"+e+"</span>"});

            $("#empresas").html(selected);
            console.log(selected);
        }
        $(document).ready(function(){
            alert("asdas");
    console.log("asdasdasda");
            $("#nomeUsuario").after("<span class='fas fa-exclamation iconUsuarioExclamation' id='spinner'></span>");
            $('#BtnEnviaForm').on('click', function () {
                let dados =  [
                                {"login":"joao"},
                                {"user_dataflex":"xXJOAOXx"},
                                {"senha":"123"},
                                {"upload":"imagem"},
                                {"nome_empresa":"FILIAL"}
                            ];
                $.ajax({
                    url: "../service_secUser.php",
                    type: "GET",
                    data: {"login_param": dados },
                    dataType: "JSON",
                    success:function(data){
                        console.log(data);
                    }
                });
            });
            $(document).on('change', '.btn-file :file', function() {
            var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
            });

            $('.btn-file :file').on('fileselect', function(event, label) {
                
                var input = $(this).parents('.input-group').find(':text'),
                    log = label;
                
                if( input.length ) {
                    input.val(log);
                } else {
                    if( log ) alert(log);
                }
            
            });
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    
                    reader.onload = function (e) {
                        $('#img-upload').attr('src', e.target.result);
                    }
                    
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#imgInp").change(function(){
                readURL(this);
            });
        });
        $(document).on('keyup', '#nomeUsuario', function() {
            $("#spinner").removeClass();
            $('#spinner').addClass('fas fa-spinner fa-pulse iconUsuarioLoad');
            let usuario = $("#nomeUsuario").val();

            console.log(usuario);
            $.ajax({
                    url: "../blank_service_secUser.php",
                    type: "GET",
                    data: {"procuraUsuario": usuario },
                    dataType: "JSON",
                    success:function(data){
                        if(data.success == 200){
                            console.log(data);  
                            $("#spinner").removeClass();
                            $('#spinner').addClass('fas fa-check iconUsuarioCheck');
//                            $('#BtnEnviaForm').attr('disabled','disabled');
                        }
                        if(data.alert == 404){
                            console.log(data);
                            $("#spinner").removeClass();
                            $('#spinner').addClass('fas fa-times iconUsuarioError');
                        }

                    }
                });
        });
    </script>
    </body>

</html>

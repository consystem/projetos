<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<style>
  #footer {
    margin: 0;
    width: 100%;
    position: fixed;
    bottom: 0;
    background: white;
    line-height: 2;
    text-align: center;
    color: black;
    font-size: 30px;
    font-family: sans-serif;
    text-shadow: 0 1px 0 #84BAFF;
    box-shadow: 0 0 8px #00214B
  }
  #tabelaCabecalho{
    background-color: lightblue;
  }
  #campoSitoserv{
   background-color: #0080FF; 
   width: 80%;
  }

  th,td{
    text-align: center;
   
  }
  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
      padding: 4px !important; 
  }

  table.table-bordered{
      border:1.5px solid black;
      margin-bottom: 20%;
    }
  table.table-bordered > thead > tr > th{
      border:1.5px solid black;
  }
  table.table-bordered > tbody > tr > td{
      border:1.5px solid black;
  }
 .fa, .fas {
    font-weight: 900;
    padding-top: 3.5px;
  } 
  ul{
    list-style-type: none;
    text-align: left;
  }
</style>


<?php
function tabulaConteudo($row,$classeOS,$statusOS,$situOS){
  if(sizeof($row['os_concluida']==0)){
    $os = "NÃO INDICADO";
  }elseif($row['os_concluida'] == 'S'){
    $os = "CONCLUIDA";
  }
  echo $row['situserv'];
  //recebe Servico @row atual
  if($row['classerv'] == 'L'){
    $classeOS[$row['classerv']] = "<i style='color:".$situOS[$row['sitoserv']]."' class='fas fa-square-full fa-lg'></i>"; 
  }else{
    $classeOS[$row['classerv']] = "<i style='color:".$situOS[$row['sitoserv']]."' class='fas fa-circle fa-lg'></i>"; 
  }
  
  //recebe status O.S da @row atual
 // $statusOS = "<span class='badge' id='campoSitoserv'>".$row['sitoserv']."</span>";
  $formata = "<tr>";
  $formata .= "   <td>".$row['agendamento_entrega']."</td>";
  $formata .= "   <td>".$row['previsao_termino']."</td>";
  $formata .= "   <td>".$row['cliente']."</td>";
  $formata .= "   <td>".$row['placa_veic']."</td>";
  $formata .= "   <td>".$row['cdordser']."</td>";
  //ClasseOS R - Revisao
  $formata .= "   <td>".$classeOS['R']."</td>";
  //ClasseOS M - Mecanica
  $formata .= "   <td>".$classeOS['M']."</td>";
  //ClasseOS E - Eletrica
  $formata .= "   <td>".$classeOS['E']."</td>";
  //ClasseOS F -Tapecaria
  $formata .= "   <td>".$classeOS['F']."</td>";
  //ClasseOS A - Alinhamento
  $formata .= "   <td>".$classeOS['A']."</td>";
  //ClasseOS P - ?
  $formata .= "   <td>".$classeOS['P']."</td>";
  // $formata .= "<td>" .$statusPS['']
  //$formata .= "   <td>".$row['sitoserv']."</td>";
  $formata .= "   <td><span class='badge' id='campoSitoserv'> ".$os." </span></td>";

  //ClasseOS L - Lavagem
  $formata .= "   <td>".$classeOS['L']."</td>";
  $formata .= "</tr>";
  return $formata;
}

function criaCabecalho(){
  $cabecalho = "<thead>";
  $cabecalho .= "   <tr id='tabelaCabecalho'>";
  $cabecalho .= "       <th>Agendamento entrega</th>";
  $cabecalho .= "       <th>Previsão término</th>";
  $cabecalho .= "       <th>Cliente</th>";
  $cabecalho .= "       <th>Placa</th>";
  $cabecalho .= "       <th>OS</th>";
  $cabecalho .= "       <th>Rev.</th>";
  $cabecalho .= "       <th>Mec.</th>";
  $cabecalho .= "       <th>Elé.</th>";
  $cabecalho .= "       <th>Tap.</th>";
  $cabecalho .= "       <th>Alin.</th>";
  $cabecalho .= "       <th>Bal.</th>";
  //$cabecalho .= "       <th>Diag.</th>";
  $cabecalho .= "       <th>Status da OS</th>";
  $cabecalho .= "       <th>Lav.</th>";  
  $cabecalho .= "   </tr>";
  $cabecalho .= "</thead>";
  return $cabecalho;
}

function footer(){

  $rodape = "   <div class='row' id='footer'>";
  $rodape .= "     <div class='col-md-6'>";
  $rodape .= "         <p>Serviços</p>";
  $rodape .= "         <ul>";
  $rodape .= "              <li class='col-md-12'><span class='fas fa-circle'style='color:yellow'></span> Amarelo = Em andamento</li>";
  $rodape .= "              <li class='col-md-12'><span class='fas fa-circle' style='color:red'></span> Vermelha = Atrasado</li>";
  $rodape .= "              <li class='col-md-12'><span class='fas fa-circle' style='color:green'></span> Verde = Concĺuído</li>";
  $rodape .= "              <li class='col-md-12'><span class='fas fa-circle' style='color:gray'></span> Cinza = Não iniciado</li><br>";
  $rodape .= "          </ul>";
  $rodape .= "       </div>";
  $rodape .= "     <div class='col-md-6'>";
  $rodape .= "         <p>Lavagem</p>";
  $rodape .= "         <ul>";
  $rodape .= "              <li class='col-md-12'><span class='fas fa-square-full' style='color:red'></span> Atrasado</li>";
  $rodape .= "              <li class='col-md-12'><span class='fas fa-square-full' style='color:yellow'></span> Realizado(Com retorno)</li>";
  $rodape .= "              <li class='col-md-12'><span class='fas fa-square-full' style='color:green'></span> Concluído</li>";
  $rodape .= "              <li class='col-md-12'><span class='fas fa-square-full' style='color:blue'></span> Aguardando</li><br>";
  $rodape .= "         </ul>";
  $rodape .= "      </div>";
  $rodape .= "  </div>";
  //$rodape .= "</div>";

  return $rodape;
}
$statusOS =[
  "S"=>"",
  "A"=>"",
];
$classeOS = [
  "E"=>"",
  "F"=>"",
  "L"=>"",
  "M"=>"",
  "P"=>"",
  "R"=>"",
  "T"=>"",
  "X"=>"",
  "Z"=>"",
];
$situOS = [
  "A"=>"gray",
  "F"=>"green",
  "P"=>"yellow",
  "C"=>"red",
];

try{
  $periodo = 1;

  $conn= new PDO("pgsql:host=devdb.cxzqmawggomt.us-east-1.rds.amazonaws.com;port=5432;dbname=pw_db_demo_demo;user=postgres;password=Cpasscsy-2107") or die('sem conexao');

  $sql = $conn->query("SELECT 
    ag.dt_agenda,
    concat_ws(' '::text, ag.dt_agenda, ag.hr_agenda) AS agendamento_entrega,
    concat_ws(' '::text, ag.dt_prometida, ag.hr_prometida) AS previsao_termino,
    ag.nome_cliente AS cliente,
    ag.placa_veic,
    ag.codservico,
    ag.cdordser,
    s605.descserv,
    s605.classerv,
    s600.os_concluida,
    s600.sitoserv
   FROM sx.sirca096 ag
     INNER JOIN sx.sirca605 s605 ON ARRAY[s605.cdfilial::text, s605.codiserv::text, s605.cdordser::text, s605.tipopedi::text] = ARRAY[ag.cdfilial::text, ag.codservico::text, ag.cdordser::text, ag.tipopedi::text]
     INNER JOIN sx.sirca600 s600 ON ARRAY[ag.cdfilial::text, ag.tipopedi::text, ag.cdordser::text, ag.cdsubord::text] = ARRAY[s600.cdfilial::text, s600.tipopedi::text, s600.cdordser::text, s600.cdsubord::text]
  WHERE ag.dt_agenda >= (now() - '".$periodo." days'::interval)
  ORDER BY ag.dt_agenda, ag.hr_agenda DESC;");

  
  $tabela =  "<table class='table table-bordered'>" .criaCabecalho();

//interação para cada linha da query retornada
  while($rowFetched = $sql->fetch(PDO::FETCH_ASSOC)){
  
    $tabela .= tabulaConteudo($rowFetched,$classeOS,$statusOS,$situOS);
  
  }
  $tabela .= "</table>";

  echo $tabela;
  echo footer();
  
}catch(Exception $e){
  echo $e;
}

?>
